<?php

class Base {

	static protected $conn = false;
	public $affected_rows = 0;     
	public $last_insertid;         
	public $lsite;                 
	public $site;                  
	public $protocol;              
	public $ip;                    
	public $eror = false;          

	public $rowsperpage = 15;
	public $range = 3;       
	public $currentpage = 1; 
	public $linkspgs = '';    
	public $totalrows = 0;

	protected $startrow;     
	protected $totalpages = 0;

	private $url = 'https://android.googleapis.com/gcm/send';
  
	public function __construct() {
		$this->ip = isset($_COOKIE['ip']) ? $_COOKIE['ip'] : $_SERVER['REMOTE_ADDR'];

		if(!isset($_COOKIE['ip']) && !headers_sent()) setcookie('ip', $this->ip, time()+60*60*24*100, '/');

		$this->site = $_SERVER['SERVER_NAME'];
		$this->protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? 'https://' : 'http://';
	}

	protected function setConn() {
		try {
			//if(class_exists('PDO')) echo "exist";
			self::$conn = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
			self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			self::$conn->exec('SET character_set_client="utf8",character_set_connection="utf8",character_set_results="utf8"; ');
			//echo self::$conn;
		}
		catch(PDOException $e) {
			$this->setEror(ERROR_MYSQL. $e->getMessage());
		}
	}

	public function sqlExecute($sql) {
		if(self::$conn===false OR self::$conn===NULL) $this->setConn(); 
		$re = true;

		if(self::$conn !== false) {

			$ar_mode = explode(' ', trim($sql), 2);
			$mode = strtolower($ar_mode[0]);
			try {
				if($sqlprep = self::$conn->prepare($sql)) {
					if($sqlprep->execute()) {
						if($mode == 'select') {
							$re = array();
							if(($row = $sqlprep->fetch(PDO::FETCH_ASSOC)) !== false){
								do {
									foreach($row AS $k=>$v) {
										if(is_numeric($v)) $row[$k] = $v + 0;
									}
									$re[] = $row;
								} while($row = $sqlprep->fetch(PDO::FETCH_ASSOC));
							}
							$this->affected_rows = count($re);
						} else $this->affected_rows = $sqlprep->rowCount();
						
						if($mode == 'insert') $this->last_insertid = self::$conn->lastInsertId();
					} else $this->setEror(ERROR_SENTENCE_SLQ);
				}
				else {
					$eror = self::$conn->errorInfo();
					$this->setEror('Error: '. $eror[2]);
				}
			}
			catch(PDOException $e) {
				$this->setEror($e->getMessage());
			}
		}

		if($this->eror !== false) $re = false;
		return $re;
	}

	public function send_notification($registatoin_ids, $message) {

		$fields = array(
		    'registration_ids' => $registatoin_ids,
		    'data' => $message,
		);

		$headers = array(
		    'Authorization: key=' . GOOGLE_API_KEY,
		    'Content-Type: application/json'
		);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		$result = curl_exec($ch);
		if ($result === FALSE) {
		    die('Ha fallado CURL: ' . curl_error($ch));
		}

		curl_close($ch);
		echo $result;
	}

	public function setEror($eror) {
		$this->eror = '<div class="eror">'. $eror. '</div>';
    	return $this->eror;
  }
}

?>