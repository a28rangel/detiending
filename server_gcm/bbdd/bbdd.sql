-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 10-07-2013 a las 12:11:49
-- Versión del servidor: 5.5.29
-- Versión de PHP: 5.3.19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `malabau_bbdd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE IF NOT EXISTS `actividad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipos_de_actividad` int(11) NOT NULL,
  `id_escuela` int(11) NOT NULL,
  `ciudad` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `pais` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `num_days` int(11) NOT NULL,
  `titulo` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `mini_descripcion` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `precio` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `equipamiento` int(11) NOT NULL,
  `edad_min` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `edad_max` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `num_max_participantes` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `nivel` int(11) NOT NULL,
  `reembolso` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`id`, `id_tipos_de_actividad`, `id_escuela`, `ciudad`, `pais`, `fecha_inicio`, `fecha_fin`, `num_days`, `titulo`, `mini_descripcion`, `descripcion`, `precio`, `equipamiento`, `edad_min`, `edad_max`, `num_max_participantes`, `nivel`, `reembolso`) VALUES
(11, 1, 1, 'Tarifa', ' España', '2013-06-30', '2013-08-22', 5, 'Titulo', 'mini descripcion', 'Descripción larga de una actividad', '100', 1, '5', '75', '5', 1, 0),
(12, 7, 1, 'Navacerrada', ' España', '2013-12-30', '2014-03-30', 5, 'Curso avanzado de Snow', 'Es un curso impresionante. No te lo puedes dejar escapar!.', 'Descripción larga', '100', 1, '5', '70', '10', 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blques_actividades`
--

CREATE TABLE IF NOT EXISTS `blques_actividades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `imagen_1` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `imagen_2` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `color` varchar(7) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `blques_actividades`
--

INSERT INTO `blques_actividades` (`id`, `nombre`, `imagen_1`, `imagen_2`, `color`, `descripcion`) VALUES
(1, 'Playa', 'blq1.jpg', 'playa.jpg', '#22bfec', 'Disfruta de la playa como nunca antes. Elige que <strong>actividad de playa</strong> quieres conocer y disfrutar.'),
(2, 'Montana', 'blq2.jpg', 'montana.jpg', '#d6811c', 'Disfruta de la montaña como nunca antes. Elige que <strong>actividad de montaña</strong> quieres conocer y disfrutar.'),
(3, 'Camps', 'blq3.jpg', 'camps.jpg', '#f2da13', 'Disfruta del camps como nunca antes. Elige que <strong>actividad de camps</strong> quieres conocer y disfrutar.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_AppClub`
--

CREATE TABLE IF NOT EXISTS `brain_AppClub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL DEFAULT '0',
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `brain_AppClub`
--

INSERT INTO `brain_AppClub` (`id`, `id_padre`, `id_tipo`, `nombre`, `fecha`, `activo`, `orden`) VALUES
(1, 0, 1, 'Instalaciones', '2013-02-27', '1', 1),
(2, 0, 1, 'Servicios', '2013-02-27', '1', 2),
(3, 0, 1, 'Actividades', '2013-02-27', '1', 3),
(4, 0, 1, 'Localizacion', '2013-02-27', '1', 4),
(5, 0, 1, 'Restaurante', '2013-02-27', '1', 5),
(6, 0, 1, 'Tienda', '2013-02-27', '1', 6),
(7, 1, 2, 'Contenido', '2013-02-27', '1', 1),
(8, 2, 2, 'Contenido', '2013-02-27', '1', 1),
(9, 3, 2, 'Contenido', '2013-02-27', '1', 1),
(10, 4, 2, 'Contenido', '2013-02-27', '1', 1),
(11, 5, 2, 'Contenido', '2013-02-27', '1', 1),
(12, 6, 2, 'Contenido', '2013-02-27', '1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_AppCursos`
--

CREATE TABLE IF NOT EXISTS `brain_AppCursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL DEFAULT '0',
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `brain_AppCursos`
--

INSERT INTO `brain_AppCursos` (`id`, `id_padre`, `id_tipo`, `nombre`, `fecha`, `activo`, `orden`) VALUES
(1, 0, 1, 'Curso1', '2013-02-27', '1', 1),
(2, 1, 2, 'Tema1', '2013-02-27', '1', 1),
(3, 1, 2, 'Tema2', '2013-02-27', '1', 2),
(4, 1, 2, 'Tema3', '2013-02-27', '1', 3),
(5, 0, 1, 'Curso2', '2013-03-01', '1', 2),
(6, 1, 2, 'Tema4', '2013-03-01', '1', 4),
(7, 1, 2, 'Tema5', '2013-03-01', '1', 5),
(8, 0, 1, 'Curso3', '2013-03-01', '1', 3),
(9, 0, 1, 'Curso4', '2013-03-01', '1', 4),
(10, 0, 1, 'Curso5', '2013-03-01', '1', 5),
(11, 0, 1, 'Curso6', '2013-03-01', '1', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_AppDependencias`
--

CREATE TABLE IF NOT EXISTS `brain_AppDependencias` (
  `id_padre` int(11) NOT NULL,
  `id_hijo` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `brain_AppDependencias`
--

INSERT INTO `brain_AppDependencias` (`id_padre`, `id_hijo`) VALUES
(0, 1),
(1, 2),
(1, 14),
(0, 3),
(3, 4),
(3, 14),
(0, 5),
(5, 6),
(5, 14),
(0, 7),
(7, 8),
(7, 14),
(0, 9),
(9, 10),
(9, 11),
(12, 13),
(0, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_AppElemento`
--

CREATE TABLE IF NOT EXISTS `brain_AppElemento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL DEFAULT '0',
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_AppEmbarcacion`
--

CREATE TABLE IF NOT EXISTS `brain_AppEmbarcacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL DEFAULT '0',
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `brain_AppEmbarcacion`
--

INSERT INTO `brain_AppEmbarcacion` (`id`, `id_padre`, `id_tipo`, `nombre`, `fecha`, `activo`, `orden`) VALUES
(1, 0, 1, 'Varadero', '2013-03-05', '1', 1),
(2, 0, 1, 'Tripulacion', '2013-03-05', '1', 2),
(3, 0, 1, 'Mantenimiento', '2013-03-05', '1', 3),
(4, 0, 1, 'Combustible', '2013-03-05', '1', 4),
(5, 0, 1, 'Remolque', '2013-03-05', '1', 5),
(6, 0, 1, 'Parking', '2013-03-05', '1', 6),
(7, 0, 1, 'Basicos', '2013-03-05', '1', 7),
(8, 7, 2, 'Luz', '2013-03-19', '1', 1),
(9, 7, 2, 'Aceites', '2013-03-19', '1', 2),
(10, 7, 2, 'Agua', '2013-03-19', '1', 3),
(11, 7, 2, 'Basuras', '2013-03-19', '1', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_AppEventos`
--

CREATE TABLE IF NOT EXISTS `brain_AppEventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL DEFAULT '0',
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `brain_AppEventos`
--

INSERT INTO `brain_AppEventos` (`id`, `id_padre`, `id_tipo`, `nombre`, `fecha`, `activo`, `orden`) VALUES
(1, 0, 1, 'Evento1', '2013-02-27', '1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_AppMagazine`
--

CREATE TABLE IF NOT EXISTS `brain_AppMagazine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL DEFAULT '0',
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `brain_AppMagazine`
--

INSERT INTO `brain_AppMagazine` (`id`, `id_padre`, `id_tipo`, `nombre`, `fecha`, `activo`, `orden`) VALUES
(1, 0, 1, 'magazine Enero', '2013-01-01', '1', 1),
(2, 0, 1, 'magazine Febrero', '2013-02-01', '1', 2),
(4, 1, 2, 'noticia2', '2013-01-01', '1', 1),
(5, 1, 2, 'noticia3', '2013-01-01', '1', 2),
(6, 1, 2, 'noticia4', '2013-01-01', '1', 3),
(7, 2, 2, 'noticia1', '2013-01-01', '1', 1),
(8, 2, 2, 'noticia2', '2013-01-01', '1', 2),
(9, 2, 2, 'noticia3', '2013-01-01', '1', 3),
(10, 2, 2, 'noticia4', '2013-01-01', '1', 4),
(11, 0, 1, 'magazine Marzo', '2013-01-01', '0', 4),
(12, 0, 1, 'magazine Abril', '2013-02-26', '0', 3),
(13, 11, 2, 'noticia1', '2013-02-26', '0', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_AppTipos`
--

CREATE TABLE IF NOT EXISTS `brain_AppTipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `icono` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `multiidioma` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `archivos` int(11) NOT NULL,
  `perfil_read` int(11) NOT NULL,
  `perfil_edit` int(11) NOT NULL,
  `perfil_create` int(11) NOT NULL,
  `perfil_delete` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `brain_AppTipos`
--

INSERT INTO `brain_AppTipos` (`id`, `nombre`, `titulo`, `icono`, `multiidioma`, `archivos`, `perfil_read`, `perfil_edit`, `perfil_create`, `perfil_delete`) VALUES
(1, 'club', 'club', 'icon-screenshot', '0', 0, 5, 2, 1, 1),
(2, 'club_contenido', 'contenido', 'icon-file-alt', '0', 0, 5, 3, 2, 2),
(3, 'cursos', 'seccion', 'icon-book', '0', 0, 5, 2, 1, 1),
(4, 'cursos_contenido', 'contenido', 'icon-file-alt', '0', 0, 5, 3, 2, 2),
(5, 'embarcacion', 'seccion', 'icon-flag', '0', 0, 5, 2, 1, 1),
(6, 'embarcacion_contenido', 'contenido', 'icon-file-alt', '0', 0, 5, 3, 2, 2),
(7, 'eventos', 'listado', 'icon-calendar', '0', 0, 5, 2, 1, 1),
(8, 'eventos_contenido', 'evento', 'icon-calendar', '0', 0, 5, 3, 2, 2),
(9, 'magazine', 'revista', 'icon-rss', '0', 0, 5, 2, 1, 1),
(10, 'magazine_noticias', 'pagina', 'icon-file-alt', '0', 0, 5, 3, 2, 2),
(11, 'magazine_publicidd', 'publicidad', 'icon-file-alt', '0', 0, 5, 3, 2, 2),
(12, 'listado_tarifas', 'listado tarifas', 'icon-list', '0', 0, 5, 2, 1, 1),
(13, 'articulo', 'articulo', 'icon-circle', '0', 0, 5, 2, 1, 1),
(14, 'formulario', 'formulario', 'icon-list-alt', '0', 0, 5, 3, 2, 2),
(15, 'meteo', 'config meteo', 'icon-cloud', '0', 0, 5, 2, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_club`
--

CREATE TABLE IF NOT EXISTS `brain_club` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `brain_club`
--

INSERT INTO `brain_club` (`id`, `id_padre`, `titulo`, `imagen`) VALUES
(1, 1, 'Instalaciones', '1_club_imagen.jpg'),
(2, 2, 'Servicios', '2_club_imagen.jpg'),
(3, 3, 'Actividades', '3_club_imagen.jpg'),
(4, 4, 'Localizaci&oacute;n', '4_club_imagen.jpg'),
(5, 5, 'Restaurante', '5_club_imagen.jpg'),
(6, 6, 'Tienda', '6_club_imagen.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_club_contenido`
--

CREATE TABLE IF NOT EXISTS `brain_club_contenido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `brain_club_contenido`
--

INSERT INTO `brain_club_contenido` (`id`, `id_padre`, `titulo`, `texto`, `imagen`) VALUES
(1, 7, 'Instalaciones', 'Capitan&iacute;a<br>La capitan&iacute;a del Club Nautico est&aacute; situada al final del muelle central. Es un edificio de dos plantas donde radica la administraci&oacute;n del Club y la direcci&oacute;n.<br>El horario de atenci&oacute;n es de lunes a viernes de 9 a 13 y de 16 a 19 horas, domingos y festivos de 9 a 13 horas.<br>&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; <br>&nbsp;<br>Mapa de pantalanes<br>En el mapa encontrar&aacute; ubicadas las distintas instalaciones del Club: restaurante, bar, piscina, ba&ntilde;os, varadero, explanada de vela, capitan&iacute;a, oficina deportiva, gasolinera, etc.<br>&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; <br>&nbsp;<br>Varadero<br>El varadero es el lugar para las reparaciones de las embarcaciones. El Club ofrece servicio de limpieza y pintura, concertado con industriales y colaboradores, mec&aacute;nica, mantenimiento de interiores, etc.<br>El varadero tiene un puente-gr&uacute;a de 30 toneladas y una gr&uacute;a de 4 toneladas.<br>El equipo del varadero est&aacute; formado por cuatro profesionales altamente cualificados que le ofrecer&aacute;n un servicio r&aacute;pido y eficiente.<br>&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; <br>&nbsp;<br><br>La piscina<br>La piscina del Club situada al lado del restaurante, ofrece sus servicios desde los primeros calores del a&ntilde;o hasta los primeros fr&iacute;os. Tiene una zona ajardinada y cuenta con un vigilante.<br>&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; <br>&nbsp;<br><br>Restaurante<br>El restaurante y bar situados en la entrada del Club, ofrece servicio de men&uacute; y carta, servicio de bar.<br>El bar tiene una zona de terrazas de m&aacute;s de 250 metros cuadrados con un entorno envidiable.<br>&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; <br>&nbsp;<br><br>Zona de reciclaje medioambiental<br>En el varadero del Club est&aacute; situada la zona de reciclaje para residuos especiales. Tiene contenedores para aceites, envases de aceites, restos de pinturas, &aacute;nodos, disolventes, envases contaminados, bater&iacute;as, lodos y aerosoles. Todos estos materiales se env&iacute;an a vertederos controlados.<br>Adem&aacute;s al lado de la puerta de acceso hay una zona de reciclaje de cart&oacute;n, pl&aacute;sticos y cristal dom&eacute;stico.<br>', '7_club_contenido_imagen.jpg'),
(2, 8, 'Servicios', 'En el Club N&aacute;utico disponemos de todos los servicios necesarios\r\n para que su estancia sea lo mas confortable posible. Descargue-se el \r\nPDF de tarifas para transeuntes y si desea reservar un amarre, rellene \r\nel formulario que encontrar&aacute; m&aacute;s abajo y nos pondremos en contacto con \r\nusted en el menor tiempo posible.', '8_club_contenido_imagen.jpg'),
(3, 9, 'Actividades', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget \r\ndiam mauris, eu malesuada lorem. Proin felis enim, convallis ac lacinia \r\nac, ultrices at neque. Ut porttitor porta turpis, ac molestie arcu porta\r\n at. Cras sit amet mi sit amet magna volutpat auctor. Aliquam tempus \r\nelementum erat, dignissim facilisis ante fringilla sed. Nulla a purus \r\neget leo accumsan blandit ut eu massa. Mauris volutpat sapien sit amet \r\norci tincidunt non tristique ligula dictum.\r\n</p>\r\n<p>\r\nCras elementum justo nec leo euismod et imperdiet velit molestie. Cum \r\nsociis natoque penatibus et magnis dis parturient montes, nascetur \r\nridiculus mus. Pellentesque vitae tortor lectus, ut pharetra magna. \r\nPhasellus semper mollis lacinia. Ut posuere turpis sit amet tortor \r\nvehicula eget sollicitudin tortor lacinia. Maecenas fringilla fermentum \r\nmagna. Quisque scelerisque convallis eros, mollis tempus erat sodales a.\r\n Nunc pulvinar adipiscing est, sit amet vulputate justo ornare quis. \r\nPellentesque habitant morbi tristique senectus et netus et malesuada \r\nfames ac turpis egestas. Suspendisse congue porttitor velit, sed \r\npulvinar mauris aliquet eget.\r\n</p>', '9_club_contenido_imagen.jpg'),
(4, 10, 'Localizaci&oacute;n', '<br>', ''),
(5, 11, 'Restaurante', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget \r\ndiam mauris, eu malesuada lorem. Proin felis enim, convallis ac lacinia \r\nac, ultrices at neque. Ut porttitor porta turpis, ac molestie arcu porta\r\n at. Cras sit amet mi sit amet magna volutpat auctor. Aliquam tempus \r\nelementum erat, dignissim facilisis ante fringilla sed. Nulla a purus \r\neget leo accumsan blandit ut eu massa. Mauris volutpat sapien sit amet \r\norci tincidunt non tristique ligula dictum.\r\n</p>\r\n<p>\r\nCras elementum justo nec leo euismod et imperdiet velit molestie. Cum \r\nsociis natoque penatibus et magnis dis parturient montes, nascetur \r\nridiculus mus. Pellentesque vitae tortor lectus, ut pharetra magna. \r\nPhasellus semper mollis lacinia. Ut posuere turpis sit amet tortor \r\nvehicula eget sollicitudin tortor lacinia. Maecenas fringilla fermentum \r\nmagna. Quisque scelerisque convallis eros, mollis tempus erat sodales a.\r\n Nunc pulvinar adipiscing est, sit amet vulputate justo ornare quis. \r\nPellentesque habitant morbi tristique senectus et netus et malesuada \r\nfames ac turpis egestas. Suspendisse congue porttitor velit, sed \r\npulvinar mauris aliquet eget.\r\n</p>', '11_club_contenido_imagen.jpg'),
(6, 12, 'Tienda', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget \r\ndiam mauris, eu malesuada lorem. Proin felis enim, convallis ac lacinia \r\nac, ultrices at neque. Ut porttitor porta turpis, ac molestie arcu porta\r\n at. Cras sit amet mi sit amet magna volutpat auctor. Aliquam tempus \r\nelementum erat, dignissim facilisis ante fringilla sed. Nulla a purus \r\neget leo accumsan blandit ut eu massa. Mauris volutpat sapien sit amet \r\norci tincidunt non tristique ligula dictum.\r\n</p>\r\n<p>\r\nCras elementum justo nec leo euismod et imperdiet velit molestie. Cum \r\nsociis natoque penatibus et magnis dis parturient montes, nascetur \r\nridiculus mus. Pellentesque vitae tortor lectus, ut pharetra magna. \r\nPhasellus semper mollis lacinia. Ut posuere turpis sit amet tortor \r\nvehicula eget sollicitudin tortor lacinia. Maecenas fringilla fermentum \r\nmagna. Quisque scelerisque convallis eros, mollis tempus erat sodales a.\r\n Nunc pulvinar adipiscing est, sit amet vulputate justo ornare quis. \r\nPellentesque habitant morbi tristique senectus et netus et malesuada \r\nfames ac turpis egestas. Suspendisse congue porttitor velit, sed \r\npulvinar mauris aliquet eget.\r\n</p>', '12_club_contenido_imagen.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_cursos`
--

CREATE TABLE IF NOT EXISTS `brain_cursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duracion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `brain_cursos`
--

INSERT INTO `brain_cursos` (`id`, `id_padre`, `titulo`, `duracion`, `descripcion`, `imagen`) VALUES
(1, 1, 'CURSO DE INICIACI&Oacute;N A LA NAVEGACI&Oacute;N A VELA', '1 semana', 'Este curso pretende hacerle conocer la navegaci&oacute;n a vela y adquirir los conceptos y conocimientos b&aacute;sicos y m&iacute;nimos para navegar.<br>', '1_cursos_imagen.jpg'),
(2, 5, 'CURSO PATR&Oacute;N DE NAVEGACI&Oacute;N B&Aacute;SICA', '3 semanas', '<br>', ''),
(3, 8, 'CURSO PATR&Oacute;N DE YATE', '3 semanas', '<br>', ''),
(4, 9, 'CURSO DE NAVEGACI&Oacute;N ELECTR&Oacute;NICA CON GPS', '1 semana', '<br>', ''),
(5, 10, 'CURSO DE NAVEGACI&Oacute;N ASTRON&Oacute;MICA', '', '<br>', ''),
(6, 11, 'CURSO DE NAVEGACI&Oacute;N COSTERA PARA PATRONES', '', '<br>', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_cursos_contenido`
--

CREATE TABLE IF NOT EXISTS `brain_cursos_contenido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `brain_cursos_contenido`
--

INSERT INTO `brain_cursos_contenido` (`id`, `id_padre`, `titulo`, `texto`, `video`, `imagen`) VALUES
(1, 2, 'TEMA 1. EL VIENTO', 'En una embarcaci&oacute;n a vela es de importancia capital saber que utilidad vamos a dar al viento reinante, como haremos incidir al mismo sobre la embarcaci&oacute;n para que &eacute;sta avance, su direcci&oacute;n y origen.<br><br>Los vientos provenientes, en nuestra zona, del 1&ordm; y 4&ordm; cuadrante son m&aacute;s frescos y secos que los provenientes del 2&ordm; y 3&ordm; cuadrante, los cuales efect&uacute;an su recorrido sobre el mar, lo que los hace m&aacute;s templados y h&uacute;medos.<br><br>El origen de los vientos generales se debe a las Borrascas y a los Anticiclones, las primeras significan una presi&oacute;n atmosf&eacute;rica baja y en las zonas por ellas abarcadas los vientos giran, en el hemisferio norte, en el sentido contrario a las agujas del reloj. Los Anticiclones, por el contrario, significan una presi&oacute;n atmosf&eacute;rica alta, conllevan buen tiempo y en la zona por ellos abarcada, en el hemisferio norte, los vientos giran en el mismo sentido que las agujas del reloj.<br><br>Si efectuamos, no obstante, un an&aacute;lisis puntual de una zona costera veremos que a nivel local las brisas de costa tienen otros or&iacute;genes, que son los debidos a la distinta capacidad de calentamiento y enfriamiento de la tierra y el agua. El mecanismo que las establece funciona tanto mejor cuanto menor sea la nubosidad.<br><br>Durante el d&iacute;a y por la acci&oacute;n solar, el suelo se calienta m&aacute;s deprisa que el agua. El aire tiene la curiosa propiedad de ser diat&eacute;rmico, lo que significa que no se calienta directamente por la acci&oacute;n de los rayos solares que lo atraviesan, los cuales, s&iacute; que calientan, aunque desigualmente el suelo y el agua. Es el contacto con &eacute;stos el que calienta el aire de las capas bajas, propag&aacute;ndose el calor hacia arriba por convecci&oacute;n (ascenso de burbujas de aire caliente, m&aacute;s ligero que el aire fr&iacute;o). En consecuencia, el aire de las capas bajas se calienta m&aacute;s que el m&aacute;s elevado y en mayor medida sobre el suelo que sobre el agua.<br><br>Como el aire m&aacute;s caliente es el que menos pesa, la presi&oacute;n atmosf&eacute;rica se hace algo m&aacute;s baja sobre el suelo que sobre el inmediato mar. Entonces, desde un punto situado en el seno del aire y sobre la vertical de l&iacute;nea de costa, la flecha que indica el sentido en que aumenta la presi&oacute;n apuntar&aacute; hacia abajo y ligeramente inclinada hacia al mar, mientras que la que indica el sentido en que aumenta la temperatura apuntar&aacute; claramente hacia el interior de la tierra. Se establecer&aacute; entonces una circulaci&oacute;n del aire a ras del suelo desde el mar a la tierra. Tal viento se conoce como brisa de mar o viraz&oacute;n.<br><br>Para que el circuito quede cerrado, el aire asciende en vertical tierra adentro, sopla en contrabrisa a cierta altura y desciende en vertical mar adentro.<br><br>Durante la noche la tierra se enfria m&aacute;s deprisa que el agua, con lo que la flecha indicadora del aumento de presi&oacute;n apuntar&aacute; hacia abajo y ligeramente inclinada hacia tierra, mientras la que indica el sentido en que aumenta la temperatura apuntar&aacute; claramente hacia el interior del mar, estableci&eacute;ndose una circulaci&oacute;n en las capas bajas de la tierra al mar. Este viento se conoce como brisa de tierra o terral.<br><br>El circuito se cierra con ascendencia (d&eacute;bil) sobre el mar contrabrisa a cierta altura y descendencia tierra adentro.<br><br>Se comprende que las brisas m&aacute;s fuertes ocurren en verano y las m&aacute;s d&eacute;biles en invierno. Pueden ser modificadas local y principalmente por una especial configuraci&oacute;n orogr&aacute;fica de las costas, por su orientaci&oacute;n y por la presencia de vegetaci&oacute;n.<br><br>La brisa de mar suele ser m&aacute;s fuerte que la de tierra; empieza como suave ventolina, arreciando progresivamente hasta los diez o doce nudos por t&eacute;rmino medio.<br><br>Ce&ntilde;ida y empopada, aqu&iacute; juegan cuatro fuerzas. <br><br>Las dos formas extremas de navegar a vela son la ce&ntilde;ida y la empopada; no puede haber nada mas distinto que los dos sistemas de fuerzas que intervienen all&iacute;. Mientras que en ce&ntilde;ida el velero est&aacute; equilibrado y tiene tendencia a mantener el rumbo (por tanto est&aacute; en equilibrio estable) en la empopada todo contribuye a que el barco se vaya de su rumbo.<br>Veamos lo que ocurre en ce&ntilde;ida: el empuje de las velas da una resultante con gran empuje lateral y poco empuje propulsivo, pero el efecto antideriva proporcionado por la quilla u orza permite que el barco ande hacia delante.<br>Confirmemos aqu&iacute; que el empuje lateral de las velas y la resistencia del perfil sumergido tienen que estar igualados, o de lo contrario el barco se desplazar&iacute;a lateralmente. Tenemos pues un primer equilibrio. Pero si decimos que el velero navega en equilibrio estable nos estamos refiriendo mas bi&eacute;n a su voluntad de mantenerse a rumbo, contra la tendencia a separarse de &eacute;l que registra cuando va en popa.<br>Imaginemos que el velero orza demasiado, ya sea por su natural tendencia, por culpa de un fallo en el tim&oacute;n o a causa de una ola: si las velas est&aacute;n cazadas correctamente, la mayor ser&aacute; la primera en sufrir el desvente producido por el g&eacute;nova. Aunque &eacute;ste se devente tambi&eacute;n en el gr&aacute;til, el centro v&eacute;lico efectivo se desplazar&aacute; hacia proa, lo cual combinado con la p&eacute;rdida de velocidad del velero que le har&aacute; deslizarse de costado sobre el agua al perder eficacia su plano de deriva contribuir&aacute;n a hacerle volver a rumbo.<br><br>Si el equilibrio v&eacute;lico es correcto, tambi&eacute;n al caer demasiado el barco, disminuir&aacute; la eficacia del g&eacute;nova, al ser tapado por la mayor. El centro v&eacute;lico se desplazar&aacute; hacia popa, dando al barco ganas de orzar y devolvi&eacute;ndole a su rumbo inicial. Este movimiento autom&aacute;tico puede no ser muy evidente en los barcos actuales debido a la gran superficie de la pala del tim&oacute;n, que en muchos casos est&aacute; compensada. Al ser el tim&oacute;n m&oacute;vil y afectar de forma tan notable al rumbo, su efecto puede sobre el equilibrio del velero. Al amarrar el tim&oacute;n en una posici&oacute;n fija se observa el equilibrio con m&aacute;s facilidad.<br><br>Si pasamos de la ce&ntilde;ida al descuartelar, al aflojar las escotas, nos encontraremos con que el barco tiene todav&iacute;a m&aacute;s tendencia a orzar. &iquest;A qu&eacute; se debe esto, si parece que al aflojar las escotas el centro v&eacute;lico se desplaza hacia delante y por tanto deber&iacute;a el barco ser m&aacute;s manejable? Un peque&ntilde;o dibujo nos permite ver como, al abrir las velas, el centro de empuje se va hacia el costado y produce un momento de giro que obliga al barco a irse de orzada. Este es un punto de desequilibrio que s&oacute;lo puede compensarse accionando el tim&oacute;n. <br><br>A)&nbsp;&nbsp;&nbsp; En navegaci&oacute;n de ce&ntilde;ida el equilibrio depende de dos pares de fuerzas opuestas. Mientras al empuje hacia delante se opone la resistencia al avance, el empuje lateral <br>es contrarrestado por la presencia de la orza.<br><br>B) Al abrir las escotas el centro de empuje se traslada al costado del barco. No es pues un desequilibrio longitudinal sino transversal el que produce este tir&oacute;n en el tim&oacute;n. <br><br>En cuanto al equilibrio navegando en popa, tambi&eacute;n la f&iacute;sica contradice la primera impresi&oacute;n y demuestra que es el m&aacute;s inestable y complejo. Asumamos que un velero navega empujado por el viento sin alcanzar su velocidad l&iacute;mite, con la mayor y el spiinaker izados. Lo primero que vemos es el equilibrio entre las dos fuerzas opuestas del empuje de las velas y el freno causado por el paso del casco en el agua. Si el empuje es mayor el barco acelerar&aacute;, al aumentar la velocidad crecer&aacute; la resistencia, de forma que cuando ambas sean iguales se producir&aacute; el equilibrio. Cualquier peque&ntilde;o incidente que desv&iacute;e el barco de su rumbo puede desequilibrarlo muy f&aacute;cilmente. Mientras el empuje del spinnaker y la mayor se encuentren en l&iacute;nea por el centro de gravedad del barco y por tanto, del punto de aplicaci&oacute;n de freno, el barco ir&aacute; recto. Pero s&oacute;lo hacen falta cinco grados de escora para que la mayor y el spy tiren apoyados en un costado y hagan orzar el barco. Si nadie corrige esto en el tim&oacute;n puede el spy llegar a vaciarse; la mayor entonces ser&aacute; la &uacute;nica fuerza de propulsi&oacute;n que har&aacute; orzar el barco hasta que &eacute;ste quede atravesado al viento. <br><br>C ) Sin tim&oacute;n ser&iacute;a imposible mantener el rumbo cuando el barco navega en popa. Las fuerzas que intervienen aqu&iacute; trabajan en planos diferenciados, haciendo mas dif&iacute;cil su equilibrio.<br>', '', '2_cursos_contenido_imagen.jpg'),
(2, 3, 'TEMA 2. RESISTENCIA A LA ESCORA I.', 'El problema, quiz&aacute;s m&aacute;s agudo, en el dise&ntilde;o de un velero es la colocaci&oacute;n del centro v&eacute;lico, o el equilibrio entre &eacute;ste y el centro de deriva. En teor&iacute;a, para que un velero est&eacute; equilibrado deber&iacute;an coincidir el empuje de las velas con la resistencia lateral, y anularse mutuamente. El sistema geom&eacute;trico m&aacute;s simple (calcular en el plano el centro geom&eacute;trico de la deriva y de las velas y hacerlas coincidir en la misma vertical) ha demostrado varias veces que no funciona. Resulta que si se quiere que el barco ci&ntilde;a tiene que ser un poco ardiente. Pero al escorar su casco cobra asimetr&iacute;a y se desplaza el empuje de las velas a sotavento, creando una tendencia a la orzada de forma natural e imposible de calcular fiablemente. Por si esto fuera poco, no es lo mismo el centro geom&eacute;trico del plano de deriva que su centro efectivo de resistencia lateral; seg&uacute;n sea su forma y espesor, el perfil tendr&aacute; un efecto distinto cuando el barco navega e incluso al variar la velocidad o el &aacute;ngulo de escora puede desplazarse el centro hacia delante o hacia atr&aacute;s. <br><br>Con las velas ocurre algo muy parecido, pues su centro geom&eacute;trico no coincide con el punto de aplicaci&oacute;n de la fuerza resultante. Este puede variar notablemente dependiendo del viento que sopla, la situaci&oacute;n de la bolsa m&aacute;xima de las velas, la interacci&oacute;n entre ellas y tambi&eacute;n la forma de navegar del timonel: si el barco va muy "lleno", algo ca&iacute;do para el viento, el centro de empuje ser&aacute; distinto del obtenido del navegar apurando el &aacute;ngulo haciendo flamear los gr&aacute;tiles. <br><br>El centro v&eacute;lico acostumbra a estar, en los cruceros modernos, ligeramente por delante del centro de deriva. Decir que la distancia var&iacute;a entre un 3% y un 8% de la eslora de flotaci&oacute;n no ayudar&aacute; mucho al lector. Estos queden quiz&aacute;s m&aacute;s satisfechos sabiendo que los dise&ntilde;adores acostumbran a utilizar el m&eacute;todo de "a ojo", como se demuestra por lo a menudo que los barcos de regata tienen que volver al astillero para modificar la posici&oacute;n de su quilla. En los barcos de serie los arquitectos cuentan con la experiencia de otros dise&ntilde;os anteriores. Casi siempre los c&aacute;lculos basados en extrapolaciones de modelos probados resultan m&aacute;s eficaces que la resoluci&oacute;n de ecuaciones y c&aacute;lculos sobre el papel.<br><br>Y ya que hemos hablado del efecto de la escora, recordemos que si un velero fuese un l&aacute;piz redondo o, para seguir el ejemplo preferido por los dise&ntilde;adores, una flecha que volase por el aire, al escorar mantendr&iacute;a su forma bajo el agua. Resulta sin embargo que tiene manga, proa y popa, y que al escorar se ensancha m&aacute;s de sotavento que de barlovento. Lo grave no es solamente esto: la quilla se desplaza entonces a barlovento y las velas a sotavento. Al navegar el barco sobre el plano horizontal formado por el agua y no sobre el plano inclinado de su l&iacute;nea de flotaci&oacute;n, aparece all&iacute; otro "momento" que tiende a hacer girar el barco de sotavento a barlovento. &iquest;C&oacute;mo se consigue navegar recto a pesar de todo? Por un lado hemos visto como el dise&ntilde;ador ha previsto adelantar el centro v&eacute;lico; luego est&aacute; el trabajo de la tripulaci&oacute;n, que deber&aacute; adelantarlo a&uacute;n m&aacute;s a base de los convenientes reglajes. El resto lo hace el timonel aguantando la rueda con fuerza. <br><br>Los dise&ntilde;adores acostumbran a hablar de barcos "r&iacute;gidos" y barcos "blandos" cuando diferencian a aquellos que aguantan mucho trapo sin escorar de los que toman excesivo &aacute;ngulo a poco que aumenta el viento. A la resistencia de la escora se le acostumbra a llamar "estabilidad", t&eacute;rmino que se usa para tantas cosas que llega a crear confusi&oacute;n. En el caso de la escora la estabilidad se obtiene de la combinaci&oacute;n de la estabilidad del casco y el peso de su lastre. Este es el llamado momento de adrizamiento que se opone al momento de escora causado por el viento sobre las velas, as&iacute; como en alg&uacute;n caso por la pendiente de las olas. En circunstancias normales, cuando el barco navega con un &aacute;ngulo de escora fijo, se produce un equilibrio entre el momento de escora y el momento de adrizamiento. Es decir, ambos son iguales. El &aacute;ngulo de escora aumenta a medida que el viento aumenta, hasta equiparar los valores y alcanzar el equilibrio. Puede ocurrir tambi&eacute;n, en caso de una ola muy fuerte, que el momento de escora sea mayor que el adrizante; el barco escor&aacute; m&aacute;s all&aacute; de su &aacute;ngulo de estabilidad m&aacute;xima y puede volcar.<br><br>Hay que recordar que la resistencia a la escora no es igual en todos los &aacute;ngulos. Cuando el barco est&aacute; plano resulta nulo, y a medida que el &aacute;ngulo aumenta crece con el momento adrizador, hasta llegar a un punto en que empieza a disminuir. Los arquitectos muestran este momento en una curva que en &aacute;ngulos peque&ntilde;os crece casi en vertical y seg&uacute;n la proporci&oacute;n entre manga y lastre, empieza a aplanarse en cuanto el barco llega a escoras importantes, con la regala cerca del agua. Despu&eacute;s de pasar por el m&aacute;ximo, la curva desciende hasta el cero y se prolonga por debajo de este valor, dando la zona de estabilidad negativa. El paso de positivo a negativo no es igual en todos los veleros y puede variar entre los 90 y los 160 grados de escora. La estabilidad negativa alcanza su punto m&aacute;ximo normalmente a los 180 grados, momento en que hay m&aacute;s resistencia a recuperar la posici&oacute;n inicial. La curva de estabilidad permite distinguir los veleros estables de los que no lo son: basta averiguar el tramo de estabilidad positiva. Si &eacute;ste llega a los 120 grados, como ocurre en la mayor&iacute;a de los veleros actuales, se puede considerar estable. Los dise&ntilde;os de regata pura puede tomar riesgos y bajar esta cifra hasta los 100 grados, mientras que los cruceros oce&aacute;nicos de gran desplazamiento gozan de estabilidad positiva hasta los 140 grados. <br>', '', '3_cursos_contenido_imagen.jpg'),
(3, 4, 'TEMA 3. RESISTENCIA A LA ESCORA II. LA ESTABILIDAD CONTRA LA ESCORA', 'Resulta agradable para el navegante que el barco pueda aguantar fuertes rachas de viento sin tumbarse sobre el agua. El lastre colocado en la parte inferior del barco (a 1, 2 o 3 metros bajo el agua) es responsable de mantenerle plano, de forma que cuando el viento empuja de costado y el palo se inclina se forman un par de fuerzas entre el centro de flotaci&oacute;n, desplazado hacia sotavento, y el centro de gravedad desplazado hacia barlovento. Al mismo tiempo, la orza o quilla tambi&eacute;n contrarresta el empuje lateral que har&iacute;a derrapar el barco de forma irremediable y no le permitir&iacute;a moverse hacia delante.<br><br>El equilibrio que se forma entre el peso del lastre y la fuerza del viento es el que hace decir a mucha gente que un velero actual es como un tentetieso. En esto el velero es un objeto de equilibrio estable, siempre y cuando se encuentre en la zona positiva de su curva de establidad. <br><br>Otros equilibrios interesantes de estudiar aunque algo m&aacute;s complejos, son los existentes entre el tim&oacute;n y la quilla y su interacci&oacute;n con el paso del agua. El estudio de estos efectos es dif&iacute;cil a causa de ese paso del agua, pues los esfuerzos var&iacute;an en cada momento seg&uacute;n la velocidad o el &aacute;ngulo de incidencia. Lo mismo ocurre cuando hablamos de las velas y su forma de actuar ante el viento. No solamente est&aacute; el aire en movimiento, sino que el soporte sobre el cual las velas trabajan tambi&eacute;n lo est&aacute; y este asimismo se ve soportado por otro medio, el agua, cuya densidad y comportamiento son distintos al del aire. <br><br>La resistencia al avance provocada por el casco y sus ap&eacute;ndices es, finalmente, uno de los puntos que m&aacute;s profundamente han intentado estudiar los dise&ntilde;adores. Aunque no se trate aqu&iacute; de un equilibrio entre dos fuerzas, es evidente que al reducirla se conseguir&aacute; una embarcaci&oacute;n m&aacute;s r&aacute;pida y manejable. El trabajo de c&aacute;lculo se complementa con la experiencia y la intuici&oacute;n en todos estos detalles; ninguno de ellos tiene una soluci&oacute;n exacta y definitiva; es la combinaci&oacute;n de todos lo que produce un velero mejor que los dem&aacute;s, o tambi&eacute;n a veces un barco que no navega y al que hay que olvidar.<br>', '', '4_cursos_contenido_imagen.jpg'),
(4, 6, 'TEMA 4. LOS NUDOS', 'Los nudos marineros deben tener como caracter&iacute;stica com&uacute;n el que sean f&aacute;ciles de hacer y, sobre todo de deshacer, por muy azocados (apretados) que est&eacute;n, deben poder deshacerse con facilidad. En los cabos (todas las "cuerdas" de un barco) se distinguen tres partes: Firme es la parte larga del cabo, Seno es cualquier arco o<br>curvatura que forme y Chicote es el extremo del cabo. GAZA.- Es un lazo o c&iacute;rculo que se forma al final de un cabo uniendo el chicote al firme por medio de un nudo, trenzado o cosido.<br><br>LASCA.- Tambi&eacute;n conocido como OCHO por su forma caracter&iacute;stica, es el ideal para el extremo de un cabo que no debe escaparse por un ollao , por un c&aacute;ncamo o polea.<br><br>LLANO.- Es el id&oacute;neo para unir dos cabos de igual mena (grosor). Es importante observar que ambos extremos de un mismo cabo pasan por el mismo lado del otro (o por debajo o por encima).<br><br>AS DE GU&Iacute;A.- Es el m&aacute;s importante de los nudos marineros, no se zafa (deshacerse por s&iacute; solo) nunca y por muy azocado que est&eacute; es f&aacute;cil de deshacer. La regla nemot&eacute;cnica que se suele emplear es la siguiente: la serpiente sale del lago, da una vuelta por detr&aacute;s del &aacute;rbol y se vuelve a meter en el lago.<br><br>BALLESTRINQUE.- Se utiliza para sujetar un cabo que trabaja (est&aacute; con tensi&oacute;n) a un objeto r&iacute;gido, por ejemplo para sujetar una defensa al guardamancebos. Solo hay que tener la precauci&oacute;n de dejar el chicote algo largo o bien asegurarlo con otra ligada.<br>', '', '6_cursos_contenido_imagen.jpg'),
(5, 7, 'TEMA 5. MAGNETISMO TERRESTRE', 'DECLINACION MAGNETICA O VARIACION LOCAL<br>Es el desv&iacute;o que provoca la tierra (que hace de im&aacute;n) afectando a la aguja de nuestra embarcaci&oacute;n<br>La aguja indicar&aacute; el norte magn&eacute;tico<br>Es igual para todos los barcos que est&eacute;n en esa zona<br>Las l&iacute;neas isog&oacute;nicas o is&oacute;gonas son l&iacute;neas que une puntos de igual Dm<br>Los desv&iacute;os a W son Negativos y a E son positivos<br><br>F&oacute;rmulas:<br>Rv = Rm + dm<br>Rm = Rv - dm<br><br><br>ACTUALIZACI&Oacute;N DE LA DECLINACI&Oacute;N MAGN&Eacute;TICA<br><br>Cada a&ntilde;o varia la declinaci&oacute;n magn&eacute;tica debido al movimiento de los polos magn&eacute;ticos, por ese motivo hay que actualizarla para el a&ntilde;o que estamos.<br><br><br>DESV&Iacute;O<br><br><br>Es el &aacute;ngulo que se desv&iacute;a la aguja debido a la propia acci&oacute;n magn&eacute;tica del buque<br>Cada buque tiene un desv&iacute;o diferente<br>Para navegar hemos de corregir este desv&iacute;o (tabla de desv&iacute;os)<br>Si es hacia el W (-) y hacia el E (+)<br><br>F&oacute;rmulas:<br>Rv = Ra + Ct<br>Ra = Rv - Ct<br><br><br><br>CORRECCION TOTAL (Ct)<br><br><br>Es la suma de la declinaci&oacute;n magn&eacute;tica + Desv&iacute;o. El m&aacute;ximo error que tendremos al navegar<br><br>F&oacute;rmulas:<br>Ct = dm + desv&iacute;o <br><br><br><br>OBTENCION DE LA CORRECCION TOTAL (Ct) MEDIANTE 2 DEMORAS<br><br>Mediante la f&oacute;rmula:<br>Ct = Dv - Da<br><br><br><br><br>OBTENCION DE LA CORRECCION TOTAL (Ct) MEDIANTE LA POLAR<br><br>Fijando la polar en el norte Verdadero<br><br>F&oacute;rmula: <br>Ct = -Da<br><br><br>AGUJA NAUTICA<br><br>El elemento que nos orienta, indicando el rumbo; se le llama Br&uacute;jula, Aguja n&aacute;utica o Comp&aacute;s.<br>Las propiedades de la aguja son: sensibilidad y estabilidad<br>Las tormentas el&eacute;ctricas pueden inutilizar temporalmente o permanente<br>Est&aacute; formada por:<br>Rosa (es la plancha gravada con los rumbos (0 â€“ 360&ordm;)).<br>L&iacute;nea de fe (son las l&iacute;neas que indican la proa y la popa).<br>Mortero (es el cuerpo de la aguja).<br>Bit&aacute;cora (la caja, armario de madera donde va la aguja en s&iacute;).<br>', '', '7_cursos_contenido_imagen.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_dependencias`
--

CREATE TABLE IF NOT EXISTS `brain_dependencias` (
  `id_padre` int(11) NOT NULL,
  `id_hijo` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `brain_dependencias`
--

INSERT INTO `brain_dependencias` (`id_padre`, `id_hijo`) VALUES
(1, 2),
(1, 3),
(3, 4),
(1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_elemento`
--

CREATE TABLE IF NOT EXISTS `brain_elemento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL DEFAULT '0',
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `brain_elemento`
--

INSERT INTO `brain_elemento` (`id`, `id_padre`, `id_tipo`, `nombre`, `fecha`, `activo`, `orden`) VALUES
(1, 0, 1, 'Incio', '2013-02-22', '1', 1),
(2, 0, 1, 'Playa', '2013-02-22', '1', 2),
(3, 0, 1, 'Monta&ntilde;a', '2013-02-23', '1', 3),
(4, 0, 1, 'Camps', '2013-02-23', '1', 4),
(5, 1, 2, 'Texto', '2013-02-23', '1', 1),
(6, 0, 1, 'Acerca de malabau', '2013-02-27', '1', 5),
(7, 0, 1, 'C&oacute;mo funciona', '2013-06-24', '1', 6),
(8, 0, 1, 'Seguridad', '2013-06-24', '1', 7),
(9, 0, 1, 'Contacto', '2013-06-24', '1', 8),
(10, 0, 1, 'Ayuda', '2013-06-24', '1', 9),
(11, 0, 1, 'Aviso legal', '2013-07-04', '1', 10),
(12, 0, 1, 'Condiciones usuarios', '2013-07-04', '1', 11),
(13, 0, 1, 'Condiciones proveedores', '2013-07-04', '1', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_embarcacion`
--

CREATE TABLE IF NOT EXISTS `brain_embarcacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `brain_embarcacion`
--

INSERT INTO `brain_embarcacion` (`id`, `id_padre`, `titulo`, `imagen`) VALUES
(1, 1, 'Varadero', ''),
(2, 2, 'Tripulaci&oacute;n', ''),
(3, 3, 'Mantenimiento', ''),
(4, 4, 'Combustible', ''),
(5, 5, 'Remolque', ''),
(6, 6, 'Parking', ''),
(7, 7, 'B&aacute;sicos', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_embarcacion_contenido`
--

CREATE TABLE IF NOT EXISTS `brain_embarcacion_contenido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `brain_embarcacion_contenido`
--

INSERT INTO `brain_embarcacion_contenido` (`id`, `id_padre`, `titulo`, `texto`, `imagen`) VALUES
(1, 8, 'Luz', '<br>', ''),
(2, 9, 'Aceites', '<br>', ''),
(3, 10, 'Agua', '<br>', ''),
(4, 11, 'Basuras', '<br>', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_etiqueta_noticia`
--

CREATE TABLE IF NOT EXISTS `brain_etiqueta_noticia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_etiqueta` int(11) NOT NULL,
  `id_noticia` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_eventos`
--

CREATE TABLE IF NOT EXISTS `brain_eventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filtro` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `brain_eventos`
--

INSERT INTO `brain_eventos` (`id`, `id_padre`, `titulo`, `filtro`) VALUES
(1, 1, 'Evento1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_formulario_basico`
--

CREATE TABLE IF NOT EXISTS `brain_formulario_basico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `destinatario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `u_nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `u_nombre_activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_nombre_obligatorio` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `u_apellidos_activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_apellidos_obligatorio` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `u_direccion_activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_direccion_obligatorio` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `u_cp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `u_cp_activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_cp_obligatorio` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `u_localidad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `u_localidad_activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_localidad_obligatorio` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `u_provincia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `u_provincia_activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_provincia_obligatorio` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `u_pais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `u_pais_activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_pais_obligatorio` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `u_telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `u_telefono_activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_telefono_obligatorio` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `u_fax_activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_fax_obligatorio` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `u_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `u_email_activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `u_email_obligatorio` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_idiomas`
--

CREATE TABLE IF NOT EXISTS `brain_idiomas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `visible` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `brain_idiomas`
--

INSERT INTO `brain_idiomas` (`id`, `nombre`, `codigo`, `activo`, `visible`) VALUES
(1, 'espa&ntilde;ol', 'es', '1', '1'),
(2, 'english(int)', 'en', '1', '1'),
(3, 'english(us)', 'us', '0', '0'),
(4, 'portugu&ecirc;s', 'pt', '0', '0'),
(5, 'deutsch', 'de', '0', '0'),
(6, 'fran&ccedil;aise', 'fr', '0', '0'),
(7, 'italiano', 'it', '0', '0'),
(8, 'nederlands', 'nl', '0', '0'),
(9, 'catal&aacute;', 'ca', '0', '0'),
(10, 'other', 'xx', '0', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_listado`
--

CREATE TABLE IF NOT EXISTS `brain_listado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `ordenar` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fecha',
  `cuantos` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_magazine`
--

CREATE TABLE IF NOT EXISTS `brain_magazine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `brain_magazine`
--

INSERT INTO `brain_magazine` (`id`, `id_padre`, `numero`, `fecha`, `nombre`, `titulo`, `imagen`, `orden`, `activo`) VALUES
(1, 1, 1, '2013-01-01', 'nautica magazine enero', 'nautica magazine enero', '1_magazine_imagen.jpg', 1, 1),
(2, 2, 2, '2013-02-01', 'nautica magazine febrero', 'nautica magazine febrero', '2_magazine_imagen.jpg', 2, 1),
(3, 11, 3, '2013-03-01', 'nautica magazine marzo', '', '11_magazine_imagen.jpg', 3, 1),
(4, 12, 4, '0000-00-00', '', 'nautica magazine abril', '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_magazine_noticias`
--

CREATE TABLE IF NOT EXISTS `brain_magazine_noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `visibilidad` int(11) NOT NULL,
  `seccion` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entradilla` text COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `brain_magazine_noticias`
--

INSERT INTO `brain_magazine_noticias` (`id`, `id_padre`, `fecha`, `visibilidad`, `seccion`, `titulo`, `nombre`, `entradilla`, `imagen`, `contenido`) VALUES
(2, 4, '2013-02-01', 0, 'NOTICIAS', 'La Euro Laser Masters Cup 2013 va viento en popa', '', 'Importante preinscripci&oacute;n de regatistas franceses y holandeses en el Euro Laser Masters Cup 2013-V Trofeo Calella de Palafrugell, Memorial Pitus Jim&eacute;nez, que se celebrar&aacute; del 18 al 21 de abril. Entre ellos el holand&eacute;s Cesar Sierhuis, ganador de la edici&oacute;n 2011,que volver&aacute; a Calella de Palafrugell a revalidar el t&iacute;tulo.<br>', '4_magazine_noticias_imagen.jpg', 'La cita gerundense de esta prueba europea se ha convertido en una cl&aacute;sica del circuito europeo y una regata referente de la clase laser a nivel internacional. A falta de algo m&aacute;s de dos meses del inicio de las pruebas, ya se han inscrito al Euro Laser Masters Cup del Club Vela Calella una cincuentena de regatistas, procedentes de Francia, Pa&iacute;ses Bajos, B&eacute;lgica, Gran Breta&ntilde;a, Alemania, Suiza y Espa&ntilde;a.<br><br>Entre los regatistas franceses que participaran en la prueba est&aacute;n los dos veteranos Great Grand Master, De Roï¬ƒgnac y Rene Roche, del CM Hendaye, los Grand Master Jacques Sicard y Falque Olivie del COYCH de Hieres y el Master del mismo club provenzal, Bernaz. Los franceses, siempre competitivos, tiene puestas sus miras en ganar en alguna de las clasiï¬caciones de la EuroLaser Masters Cup.<br><br>Los grandes veteranos holandeses tampoco van a faltar en la prueba del Club Vela Calella, y ya est&aacute; inscrito un nutrido grupo de regatistas, encabezados por Cesar Sierhuis que intentar&aacute; conquistar el t&iacute;tulo de nuevo. Junto con el campe&oacute;n del pasado a&ntilde;o tambi&eacute;n regatear&aacute;n en Calella el Great Grand Master Henk Wittenberg, as&iacute; como otros destacados laseristas holandeses, los Grand Master, Jaap Mazereeuw y Hans Holterman, no pudiendo faltar el Master Wilmar Groenendijk.<br><br>Destacada ser&aacute; la participaci&oacute;n de otros regatistas europeos, entre ellos el brit&aacute;nico David Schoï¬eld, asiduo en este Trofeo, el belga Luc Dumonceau, el alem&aacute;n Rene Bright o el suizo Broye.<br><br>Tambi&eacute;n la inscripci&oacute;n femenina se anima. Se contar&aacute; en esta edici&oacute;n del Trofeo Calella de Palafrugell con la participaci&oacute;n de la alemana Petra Busch y d la francesa Germnanie Larroque, que ya particip&oacute; el a&ntilde;o pasado.<br><br>Buena pre-inscripci&oacute;n espa&ntilde;ola<br><br>La presencia espa&ntilde;ola tambi&eacute;n promete ser numerosa. A parte de los regatistas locales del CV Calella, encabezados por el Great Grand Master, Pere Jimenez- Maifr&eacute;n, navegar&aacute;n en esta edici&oacute;n Miquel &Aacute;lvarez, Josep Ferr&eacute;s Marc&oacute;, Ignasi Jim&eacute;nez-Gus&iacute;, Josep Maria Campi, Xavier Penas. Del CN Aiguablava viene Josep Maria Pujada.<br><br>mportante la participaci&oacute;n del grupo de laseristas procedentes del GEN de Roses, con Jordi Roca, Ernest Amat, Peter Muller, Toni Gibert ya asiduos en el circuito del Master Laser, as&iacute; como Joan Grau del CN Vilassar de Mar, Miguel Noguer del CN Masnou, Xavier Marr&oacute;n del CN Betulo y los regatistas del CN el Balis, Ricard Llonch y Xavier Boluda, los alicantinos Jos&eacute; Juan S&aacute;nchez del CN Santa Pola, y David Quesada del CN Calpe, y el asturiano Rene Vel&aacute;zquez.<br>'),
(3, 5, '2013-02-01', 0, 'BARCOS', 'Hallberg Rassy 64', '', 'El nuevo Hallberg Rassy 64 presentado este a&ntilde;o en el sal&oacute;n de D&uuml;sseldorf destaca por la seguridad y el confort a bordo que caracterizan a todos los barcos de este astillero.Dise&ntilde;ado por Germ&aacute;n Frers, es con un concepto de amplios espacios, luminosidad y visibilidad.<br>', '5_magazine_noticias_imagen.jpg', 'Que el propio due&ntilde;o de un astillero de los mejor reputaci&oacute;n del mundo, en este caso Magnus Rassy, se enorgullezca de uno de los barcos salidos de su factor&iacute;a, en este caso su HR 64, ya dice mucho. Si adem&aacute;s, este &uacute;ltimo modelo salido de la casa sueca se convierte inmediatamente en el buque insignia en una marca tan prestigiosa y reputada como Hallberg Rassy, poco queda por agregar. Como en cualquier HR, &eacute;ste 64 pies responde a los c&aacute;nones de simplicidad y lujo n&aacute;utico moderado, por lo que sorprenden m&aacute;s sus generosos espacios que su est&eacute;tica interna y externa, que est&aacute; a la altura de las habituales en los veleros de este astillero.<br><br>Donde s&iacute; llama la atenci&oacute;n es en navegaci&oacute;n. Si bien todos los Hallberg Rassy son excelentes navegantes, &eacute;ste es particularmente veloz y ligero, lo que es muy importante y un verdadero logro en un yate de casi 20 metros de largo. <br><br>Su maniobrabilidad sorprende desde el propio pantal&aacute;n. Una mano en la rueda, otra en el gas y con los pies se gobierna la h&eacute;lice de proa. S&iacute;, un bot&oacute;n en el suelo permite mover la proa a babor y otro bot&oacute;n hace lo propio para virar a estribor. No se trata de una genialidad, pero s&iacute; asombra que no se le haya ocurrido a nadie antes.<br><br>Cada mil&iacute;metro es acertado<br><br>La ba&ntilde;era central es un dec&aacute;logo de buenas soluciones dise&ntilde;adas con un doble fin: comodidad y seguridad. Pero todo ello, como ya hemos dicho, sin comprometer las prestaciones, ya que sobre el agua es un barco atl&eacute;tico, vers&aacute;til, seguro y c&oacute;modo. Gran parte de este m&eacute;rito es del dise&ntilde;ador argentino Germ&aacute;n Frers, quien desde 1988 trabaja para la casa de Hallberg-Rassy. Esos 23 a&ntilde;os le han permitido evolucionar conjuntamente con Magnus Rassy en el desarrollo de barcos que se superan ejemplar tras ejemplar.<br><br>Dispone de una cubierta limpia y despejada. Las escotillas y tragaluces est&aacute;n empotrados y el cabestrante del ancla queda oculto bajo el nivel de cubierta.En su dise&ntilde;o se contemplan las opciones para enrollador hidr&aacute;ulico, cutterstay y enrollador de g&eacute;nova bajo cubierta.<br><br>Una pasarela opcional se puede plegar en la ba&ntilde;era. En popa hay un espacio de 8,6 metros c&uacute;bicos que puede almacenar una semirr&iacute;da y una plataforma de ba&ntilde;o doble hidr&aacute;ulica<br><br>Bajo cubierta es un verdadero gran yate. Tiene toda la amplitud de una casa. La altura de pie es de entre 2,05 y 2,15 metros en todo el interior. Los grandes portillos que permite esta altura interior traen mucha luz tanto al sal&oacute;n como a las cabinas, donde lo &uacute;nico que no cabe es la palabra estrechez. Sirve de ejemplo que la cama del camarote principal mide 2,05 por 1,70 metros. <br><br>Todos los detalles muestran solidez y funcionalidad. Destaca la limpieza de los elementos y la generosa dimensi&oacute;n de cada uno de ellos. Desde el cableado de la electricidad y electr&oacute;nica a la fontaner&iacute;a. Lo mismo puede decirse de la iluminaci&oacute;n y la calefacci&oacute;n. O hasta de los toalleros de los ba&ntilde;os.<br><br>Atl&eacute;tico y veloz<br><br>El HR 64 es un yate para grandes traves&iacute;as y en cualquier tipo de mar. En viento fresco, su parabrisas brinda una protecci&oacute;n absoluta y el tim&oacute;n responde con precisi&oacute;n y fineza. Con 18 nudos de viento es f&aacute;cil superar los 10 nudos de velocidad y a&uacute;n en esas condiciones es posible gobernar el barco con una sola mano. Todos los hidr&aacute;ulicos est&aacute;n al alcance del patr&oacute;n que no necesita de ayuda extra. Como debe ser en un barco de traves&iacute;as en las que no siempre hay cuatro brazos en las guardias y casi ninguno de ellos tiene la potencia de la extremidad de un gimnasta.<br><br>Las polares marcan 9,2 nudos de velocidad del barco con 10 nudos de viento real y con un &aacute;ngulo de 75&ordm;. Con trinqueta y mayor a 25 nudos de viento real y a 110&ordm; de &aacute;ngulo, la embarcaci&oacute;n puede alcanzar los 11,5 nudos.<br><br>La motorizaci&oacute;n de serie en el Hallberg Rassy 64 es un Volvo Penta D6-280 de 280 caballos, con un dep&oacute;sito de combustible con capacidad para 1.800 litros y los tanques de agua dulce albergan 1.300 litros. Todo lo necesario para zarpar y no regresar por mucho tiempo. Y lo que es m&aacute;s importante, sin a&ntilde;orar nada de lo que quede en tierra.<br>'),
(4, 6, '2013-02-01', 1, 'CHARTER', 'Datos claves para el alquiler de barcos I', '', 'Las fechas, la zona de navegaci&oacute;n, la elecci&oacute;n del barco, la reserva y la ï¬rma del contrato son los primeros pasos. Saber cu&aacute;les son las actividades complementarias que se desean realizar y seguir algunas normas b&aacute;sicas de la vida a bordo ser&aacute;n indispensables para disfrutar de las vacaciones al m&aacute;ximo.<br>', '6_magazine_noticias_imagen.jpg', 'Una vez tomada la decisi&oacute;n de alquilar un barco para pasar las vacaciones es importante hacer un plan con los pasos a seguir para la concreci&oacute;n de la idea. Sobre todo, cuando es la primera vez que se opta por este tipo de vacaciones en las que muchos imprevistos, si no se los conoce, pueden complicar los planes iniciales. <br><br>El primer paso es establecer las fechas disponibles y la zona de navegaci&oacute;n. Las fechas pueden implicar una reducci&oacute;n sustancial en el precio a pagar si se pasa de una semana de temporada alta a una inferior. En ocasiones, esto implica simplemente partir una semana antes o una semana despu&eacute;s de lo previsto. Este calendario ser&aacute; clave, aunque no decisivo, para elegir el &aacute;rea de navegaci&oacute;n en base al clima de la zona. En algunas ocasiones, dependiendo de las actividades que se desean realizar -submarinismo, windsurf, esqu&iacute; acu&aacute;tico o pesca- ser&aacute; la zona la que determine las fechas.<br><br>Establecidos estos puntos ya se puede buscar la compa&ntilde;&iacute;a de alquiler. Nunca hay que quedarse con el primer folleto. La competencia en el mundo del ch&aacute;rter es enorme y se pueden encontrar buenos precios y alguna oferta en todo momento. Por lo tanto, hay comparar lo m&aacute;s que se pueda antes de hacer la reserva. Es recomendable realizar esta reserva unos meses antes del ch&aacute;rter.<br><br>Elegir el barco y ï¬rmar el contrato son los siguientes pasos. En estos puntos es primordial tener muy claro qu&eacute; extras son necesarios para los programas de navegaci&oacute;n y actividades complementarias que se desean realizar. Si bien es cierto que la mayor&iacute;a de las empresas tienen disponibilidad de accesorios como para satisfacer la mayor&iacute;a de las inquietudes, no ser&iacute;a la primera vez que una familia se vea obligada a remar del barco a la playa y de la playa al barco todas las vacaciones por no haber solicitado un fueraborda para el auxiliar con la debida anticipaci&oacute;n.<br><br>Un patr&oacute;n preparado<br><br>Tanto cuando se alquila la embarcaci&oacute;n con patr&oacute;n como cuando se prescinde de este profesional es muy importante respetar ciertas normas de seguridad y convivencia que evitar&aacute;n accidentes y mal ambiente a bordo. Cuando haya patr&oacute;n, ser&aacute; &eacute;l el primer interesado en preservar estos valores por lo que es muy importante prestar atenci&oacute;n a sus indicaciones tanto a la hora de embarcar, como todas las que puedan devenir durante la navegaci&oacute;n.<br><br>Para quienes alquilen el barco sin patr&oacute;n debe quedar claro que ello no implicar&aacute; un problema si, adem&aacute;s de la titulaci&oacute;n, se tienen los conocimientos correspondientes. Bastar&aacute; seguir algunas normas e lementales y aplicar mucho sentido com&uacute;n frente a los imprevistos para que el viaje sea tan agradable como cada tripulante lo imagin&oacute;.<br><br>Es muy importante que la tripulaci&oacute;n est&eacute; informada y sea participativa. Cada persona a bordo del barco debe saber d&oacute;nde est&aacute; cada elemento de seguridad y c&oacute;mo se utiliza. Un buen reparto de tareas y responsabilidades har&aacute; que todos se sientan &uacute;tiles y disfruten de la navegaci&oacute;n.<br><br>La prudencia es una gran consejera para todos los tripulantes. No vale la pena forzar ning&uacute;n aparejo ni apurar hasta una costa con poco fondo por ahorrarse un bordo. Sobre todo hay que tener en cuenta que en los barcos de alquiler aparejo y maniobra est&aacute;n pensadas para el crucero y no para ser exigidos como si de una regata se tratase. Nunca hay que olvidar, adem&aacute;s, que un barco de ch&aacute;rter pasa por muchas manos y que no siempre los clientes le dan a la embarcaci&oacute;n el trato que debieran.<br><br>Finalmente es aconsejable dejar de lado cualquier tipo de inhibiciones a la hora de llamar a la compa&ntilde;&iacute;a si surgen problemas o si no est&aacute; seguro de c&oacute;mo funciona alg&uacute;n equipo. Ellos mismos preferir&aacute;n una llamada preventiva que un desperfecto posterior. <br><br>Tripulantes adiestrados<br><br>La primera vez que se pisa un barco, y m&aacute;s si es un velero, la sensaci&oacute;n que domina es la de inseguridad. No se sabe qu&eacute; se puede pisar y qu&eacute; no, por donde pasar, d&oacute;nde ubicar los pertrechos, ni de d&oacute;nde asirse. <br><br>El tripulante deber&aacute;, sobre todo si es novato, hacer un peque&ntilde;o esfuerzo para adaptarse a los usos y costumbres de a bordo. Aprender a usar un lenguaje nuevo y a denominar a cada objeto por su nombre. A los pocos d&iacute;as, palabras como babor, estribor o trav&eacute;s, formar&aacute;n parte de su vocabulario.<br><br>Y casi sin darse cuenta lo que antes le parec&iacute;a un mundo de extraterrestres donde nada es lo que parece ni se denomina como debe, se convertir&aacute; en un hogar donde es sumamente f&aacute;cil vivir y relacionarse. Para facilitar este camino damos a continuaci&oacute;n una descripci&oacute;n de los principales espacios del barco y de c&oacute;mo desenvolverse en ellos.<br>'),
(5, 7, '2013-02-01', 0, 'NOTICIAS', 'Iv&aacute;n Pastor, Plata en el Preol&iacute;mpico de Brasil', '', 'El winsdurï¬sta del equipo Movistar Iv&aacute;n Pastor, del CN Santa Pola, ha ganado la medalla de plata en el Preol&iacute;mpico de Brasil tras lograr nueve podios parciales. La competici&oacute;n celebrada en el marco de la Semana Brasile&ntilde;a de Vela se disput&oacute; en aguas de Buzios (Brasil). Con un total de diez mangas disputadas, la cita preol&iacute;mpica ï¬naliz&oacute; una jornada antes de las seis previstas.<br>', '7_magazine_noticias_imagen.jpg', 'Tras recibir su medalla, Iv&aacute;n Pastor dijo estar satisfecho con los resultados obtenidos: "Estoy muy contento con el resultado ï¬nal. Hemos logrado completar diez mangas y todas han sido muy largas, por lo que f&iacute;sicamente han sido dur&iacute;simas. Adem&aacute;s, ha sido un gran entrenamiento porque hemos tenido vientos muy variados durante toda la semana". <br><br>Este Preol&iacute;mpico brasile&ntilde;o ha sido una gran oportunidad para Iv&aacute;n Pastor para poner en pr&aacute;ctica todo lo entrenado en los &uacute;ltimos meses, de cara a los Juegos Ol&iacute;mpicos de Londres. Estar al ciento por ciento f&iacute;sica y psicol&oacute;gicamente es ahora la m&aacute;xima prioridad del alicantino. El podio ï¬nal lo completaron los brasile&ntilde;os Ricardo Bimba, que se colg&oacute; el oro y Alberto Lopes, que ï¬naliz&oacute; en tercera posici&oacute;n. <br>'),
(6, 8, '2013-02-01', 0, 'NOTICIAS', 'Aumenta la venta de barcos', '', 'Enero de 2013 muestra un crecimiento del 12 por ciento en las ventas de embarcaciones en Espa&ntilde;a y un 73 por ciento en Catalu&ntilde;a respecto a enero de 2012. La Asociaci&oacute;n de Industrias N&aacute;uticas (ADIN) promueve festivales del mar para estimular el mercado n&aacute;utico.<br>', '8_magazine_noticias_imagen.jpg', 'Despu&eacute;s de bajadas continuas en las matriculaciones de embarcaciones en Catalu&ntilde;a y en el Estado espa&ntilde;ol, el mes de enero de 2012 cierra con cifras positivas que podr&iacute;an ser un esperado cambio de tendencia.<br><br>El pasado mes de enero 2013, se han matriculado en Catalu&ntilde;a un total de 45 embarcaciones, lo que representa un crecimiento de un 73 por ciento respeto las 26 embarcaciones del mes de enero de 2012. En el Estado espa&ntilde;ol se han matriculado un total de 249 embarcaciones, lo que representa un crecimiento de un 12 por ciento respecto a las 223 embarcaciones del mes de enero de 2012. <br><br>Con los datos facilitados por la Direcci&oacute;n General de la Marina Mercante, ADIN ha elaborado un informe en el que dice que el 83 por ciento de las embarcaciones vendidas en Catalu&ntilde;a son de hasta ocho metros y el 95 por ciento son inferiores a 12 metros. En el Estado espa&ntilde;ol el 87 por ciento son inferiores a ocho metros y el 96 por ciento inferiores a 12 metros. <br><br>Ante la situaci&oacute;n, la asociaci&oacute;n de industrias, comercio y servicios n&aacute;uticos ADIN, promover&aacute; una serie de iniciativas para dinamizar el sector n&aacute;utico, atraer a futuros compradores, impulsar las empresas dedicadas al sector y acercar la actividad n&aacute;utica a aquellos que no son profesionales. <br><br>Con esta premisa, ADIN conjuntamente con los ayuntamientos de cada localidad ha elaborado un calendario para celebrar un Festival del Mar que tendr&aacute; lugar en los puertos de Sant Feliu de Gu&iacute;xols, Port Ginesta en Sitges y Cambrils durante el mes de mayo.<br>'),
(7, 9, '2013-02-01', 0, 'TRAVES&Iacute;A', 'Un puente en la Costa Brava', '', 'La Costa Brava ofrece inï¬nidad de rincones id&iacute;licos que se pueden disfrutar en una traves&iacute;a corta en tiempo necesario, pero intensa en im&aacute;genes y experiencias. En s&oacute;lo cuatro d&iacute;as se puede recorrer y visitar la mitad de la&nbsp;&nbsp;&nbsp; Costa Brava. Unir Barcelona con las Islas Medas puede ser muy atractivo para navegantes noveles sin exponerse a los dominios de la tramontana.<br>', '9_magazine_noticias_imagen.jpg', 'Unir la costa del Maresme, al norte de Barcelona, con la Costa Brava, es una gran traves&iacute;a para quien comienza a hacer sus primeros bordos en la navegaci&oacute;n de crucero. En s&oacute;lo cuatro d&iacute;as se pueden visitar tres puertos, parar en muchas calas y ver algunos de los paisajes costeros m&aacute;s bonitos de la costa espa&ntilde;ola.<br><br>Todo ello con dos grandes alicientes para el navegante con poca experiencia: nunca se est&aacute; alejado de la costa con lo que no se requiere una titulaci&oacute;n elevada, y son s&oacute;lo cien millas que se pueden dividir en siete u ocho tramos, ma&ntilde;ana y tarde, por lo que nunca se sobrepasan las cuatro o cinco horas de navegaci&oacute;n.<br><br>La propuesta de llegar a Medas y no hasta el cabo de Creus, un destino a priori m&aacute;s apetecible, tiene un gran fundamento. De Medas a Creus, la tramontana comienza a hacerse notar y obliga a estar mucho m&aacute;s pendiente del parte meteorol&oacute;gico de lo habitual.<br><br>Echado el vistazo a la meteo, y seguros de que el barco est&aacute; en orden, se puede partir sin temores, ya que es una ruta muy segura en cuanto a puertos de recalada. La distancia m&aacute;s larga entre dos de ellos es de 15 millas entre Arenys y Blanes, por lo que el principiante siempre estar&aacute; a una horita de un amparo seguro.<br><br>Como para muestra basta un bot&oacute;n aqu&iacute; exponemos la traves&iacute;a que nos envi&oacute; nuestra lectora Meritxell Garc&iacute;a.<br><br>El centro de la Costa Brava<br><br>El s&aacute;bado por la ma&ntilde;ana pusimos rumbo a Medas. Desayunamos en el pueblo, pero, como es habitual en estas fechas, hab&iacute;a mucha gente aprovechando el puente por lo que preferimos huir de las multitudes, que era nuestro plan de ï¬n de semana. Hicimos unas millas a motor y tras dar una vuelta para ver la costa de Callella de Palafrugell, uno de los pueblos m&aacute;s emblem&aacute;ticos de este litoral, pasamos entre Llafranc y las islas Formigas, para luego apagar motor y seguir a vela.<br><br>Esta vez el viento nos daba entre la aleta de estribor y el trav&eacute;s, por lo que los diez nudos de intensidad nos empujaron hacia Medas con alegr&iacute;a. Al pasar junto al cabo de Begur, corregimos rumbo para rodear las Medas Petitas por fuera para ver la costa norte del peque&ntilde;o archipi&eacute;lago y luego fuimos hacia la Roca Foradada.<br><br>invernando en el garaje de mis padres. La tentaci&oacute;n de pasar de un lado a otro de la monta&ntilde;a por el hueco que la naturaleza ha creado debajo s&oacute;lo fue detenida a diez metros de la entrada cuando quedaba totalmente claro que el m&aacute;stil no pasaba bajo ese puente natural. Dudamos entre quedarnos fondeados en la cala o ir a puerto. Pero nos apetec&iacute;a darnos una ducha caliente y andar un poco por Estartit.<br><br>Pascua en Sa Riera<br><br>El domingo nos levantamos a las nueve y media. Est&aacute;bamos a mitad de las vacaciones, pero la sensaci&oacute;n de que todo se empieza a acabar cuando se va a iniciar el regreso no nos la quitaba nadie. Quiz&aacute;s por eso, en lugar de encarar directo al cabo de Begur pusimos proa a Sa Riera donde fondeamos. <br><br>Los bares estaban llenos, los encargados del peque&ntilde;o club n&aacute;utico comenzaban a bajar lla&uacute;ts al agua para que est&eacute;n a lla&uacute;ts al agua para que est&eacute;n a punto en las vacaciones, hab&iacute;a gente tomando el sol y hasta alg&uacute;n osado ba&ntilde;ista. Tomamos un aperitivo, comentamos los proyectos para el verano tras esta primera experiencia m&iacute;a, y disfrutamos de unas horas de vacaciones como el entorno lo ped&iacute;a. Hacia las cuatro pusimos proa a Sant Fel&iacute;u, donde llegamos a motor sobre las ocho y media. Justo para comer un pescado en el Paseo Mar&iacute;timo.<br><br>&Uacute;ltima escala<br><br>Un poco por el ï¬n inexorable del viaje, otro por justiï¬car la visita a la ciudad, nos levantamos a las siete para visitar la ermita de Sant Elm. La caminata era larga y la subida nos llevar&iacute;a unas dos horas, pero desde all&iacute; ver&iacute;amos el mar que nos separaba de casa. La vista es imponente e imperdible. Antes de las once de la ma&ntilde;ana ya vir&aacute;bamos la ermita, esta vez por mar.<br>'),
(8, 10, '2013-02-01', 0, 'CHARTER', 'Datos claves para el alquiler de barcos II', '', 'Las fechas, la zona de navegaci&oacute;n, la elecci&oacute;n del barco, la reserva y la ï¬rma del contrato son los primeros pasos. Saber cu&aacute;les son las actividades complementarias que se desean realizar y seguir algunas normas b&aacute;sicas de la vida a bordo ser&aacute;n indispensables para disfrutar de las vacaciones al m&aacute;ximo.<br>', '10_magazine_noticias_imagen.jpg', 'Una vez tomada la decisi&oacute;n de alquilar un barco para pasar las vacaciones es importante hacer un plan con los pasos a seguir para la concreci&oacute;n de la idea. Sobre todo, cuando es la primera vez que se opta por este tipo de vacaciones en las que muchos imprevistos, si no se los conoce, pueden complicar los planes iniciales. <br><br>Ba&ntilde;era<br><br>La ba&ntilde;era es el espacio de la cubierta destinado al gobierno y a la maniobra de la embarcaci&oacute;n, pero es tambi&eacute;n el espacio de convivencia al aire libre por excelencia. Como el resto de la embarcaci&oacute;n, es un espacio que debe permanecer siempre ordenado, con cada cosa en su lugar y libre de obst&aacute;culos que puedan, en un momento determinado hacer fracasar una maniobra o causar un accidente.<br><br>Lo primero que hay que aprender es d&oacute;nde est&aacute; barlovento y d&oacute;nde sotavento. Saber de d&oacute;nde viene el viento har&aacute; comprender los movimientos del barco. Lo segundo a tener en cuenta es que si no se est&aacute; completamente seguro de lo que se va a hacer, preguntar puede evitar un error que provoque un accidente.<br><br>Entre las normas de convivencia a respetar destacan tres. La primera es evitar derramar comida o bebidas en cubierta y, si ello ocurre, limpiar inmediatamente. El suelo de las ba&ntilde;eras suele acabar repleto de c&aacute;scaras de pipas, pieles de cacahuete, trozos de patatas chips y de galletas o migas de pan. Cuando se moja, el conjunto se convierte en una desagradable pasta. En verano se suele andar descalzo y nadie tiene por qu&eacute; pisar estos desperdicios.<br><br>La segunda regla es mantener el orden. Es desagradable y peligroso que una ba&ntilde;era parezca un plato de espaguetis debido al desorden de cabos. Y es lamentable y caro perder por la borda gafas, cremas solares, botellas o manetas de winche por no haberlas arranchado como corresponde en alguno de los muchos cofres o guanteras que suele haber en esta parte del barco.<br><br>La &uacute;ltima pauta tiene que ver con la convivencia, pero tambi&eacute;n con la seguridad: nunca hay que dormir en cubierta. Se molesta a los que maniobran y se corre el riesgo de caer al agua.<br><br>Para continuar con la seguridad, el tripulante novel debe saber que siempre deben estar a salvo de accidentes cabeza, manos y pies. <br><br>Aunque en los veleros de ch&aacute;rter actuales la botavara suele quedar a suï¬ciente altura, siempre hay que estar pendiente de viradas o trasluchadas repentinas para mantener lacabeza fuera de su recorrido.<br><br>Si hablamos de las manos, el peligro est&aacute; en los winches. Cuando se cazan o amollan escotas con la ayuda de estos elementos, hay que colocar la mano plana sobre ellos para no pillarse los dedos. Y para evitar accidentes en los pies es imprescindible no andar descalzo en la ba&ntilde;era. Eso no es recomendable en navegaci&oacute;n. Los dedos y las u&ntilde;as de los pies deben estar siempre protegidos. Adem&aacute;s, con un buen calzado antideslizante se evitar&aacute;n resbalones que pueden ï¬nalizar en golpes o ca&iacute;das.<br><br>Cubierta<br><br>No hay dos cubiertas iguales. Eso vale, no solo para la distribuci&oacute;n de la maniobra, sino tambi&eacute;n a la facilidad de transitar por ella o los espacios libres que pueden destinarse, por ejemplo, a tomar el sol. Hay que habituarse a circular por ella, saber d&oacute;nde colocarse, conocer el acastillaje del que hay que mantenerse alejado y, sobre todo, aprender a mantener el equilibrio.<br><br>Moverse por una cubierta plana, fondeados en una cala es f&aacute;cil. Pero cuando el barco escora y cabecea y los rociones en las amuras son constantes, las cosas ya no son sencillas. Agarrarse a determinados puntos para no perder el equilibrio puede incluso llegar a ser peligroso. Por tanto, hay que saber que en todas las cubiertas, para desplazarse de popa a proa y viceversa, existen pasamanos sobre la caseta que conï¬eren un agarre seguro. La regla b&aacute;sica dice que se debe estar cogido a un punto ï¬jo siempre: en el charco una mano para ti y la otra para el barco. <br><br>En esta zona del barco hay que estar siempre alejado de los pu&ntilde;os de las velas. Con mal tiempo, nunca hay que deambular por cubierta sin arn&eacute;s de seguridad. De noche, el arn&eacute;s es preceptivo incluso con mar plana.<br><br>Como en la ba&ntilde;era, aqu&iacute; tambi&eacute;n es aconsejable ir con calzado antideslizante. Los momentos en los que son m&aacute;s frecuentes los accidentes en cubierta son durante fondeos y atraques, y en viradas y trasluchadas. Para evitar accidentes en los fondeos hay que apartarse siempre del recorrido de la cadena o del cabo al fondear el ancla. En el &uacute;ltimo tramo de izada del ancla, cuando &eacute;sta recupera su peso real si se la sube a mano hay que adoptar una posici&oacute;n correcta de la espalda. Durante los atraques nunca hay intentar impedir que el barco toque el muelle u otro barco con el pie o las manos.<br><br>Y en las viradas y trasluchadas hay que mantenerse alejado del trayecto de la vela al pasar de una banda a otra y de los pu&ntilde;os cuando el g&eacute;nova gualdrapea. Jam&aacute;s hay que permanecer tendido en cubierta tomando el sol durante las maniobras y siempre es aconsejable estar detr&aacute;s del palo.<br><br>En cuanto a limpieza, en cubierta valen los mismos conceptos vertidos para la ba&ntilde;era. con una recomendaci&oacute;n extra para los amantes del sol. Siempre hay tenderse a tomar el sol sobre una toalla. Las cremas solares convierten las cubiertas en verdaderas pistas de patinaje.<br><br>Interior<br><br>Mientras dure la singladura, el barco ser&aacute; el hotel y el restaurante de la tripulaci&oacute;n. Eso signiï¬ca dormir, comer, asearse y hacer todo lo que se har&iacute;a en tierra, incluyendo los ratos de ocio, de lectura o de conversaci&oacute;n en torno a la mesa. Aunque no lo parezca, dentro de un barco hay mucho m&aacute;s espacio del que parece y se puede hacer todo lo que uno har&iacute;a si alquilase un apartamento para pasar sus vacaciones, pero es imprescindible saber encontrar el espacio propio y respetar el de los dem&aacute;s.<br><br>En el interior del barco las estancias est&aacute;n muy bien delimitadas seg&uacute;n los usos. En l&iacute;neas generales en un barco destinado al ch&aacute;rter hay un espacio a proa destinado a camarotes y aseos, otro central dedicado a sal&oacute;n, cocina y mesa de cartas, y otro en popa tambi&eacute;n destinado a cabinas y aseo. Cada cosa debe tener su lugar de estiba espec&iacute;ï¬co y hay que respetarlo. Por tanto cada objeto que salga de un sitio debe volver al mismo despu&eacute;s de su uso. El orden y la limpieza son fundamentales para a convivencia, al igual que lo es respetar los horarios de descanso.<br><br>Lo primero que debe saber un tripulante novato es que la energ&iacute;a el&eacute;ctrica es un bien escaso. A menos que se est&eacute; conectado a tierra o se disponga de un generador, no hay mantener las luces innecesariamente encendidas. No se debe bajar al sal&oacute;n mojado, y menos cuando reci&eacute;n se regresa de un chapuz&oacute;n en una cala chorreando agua salada, o con arena en los pies. Las v&aacute;lvulas de los sistemas de achique pueden obstruirse y se deterioran con mucha facilidad.<br><br>El segundo bien a cuidar es el agua dulce. Los recursos son limitados y, aunque se pueda repostar en un puerto cercano, en ocasiones, la operaci&oacute;n no es sencilla. Sobre todo en verano. El espacio m&aacute;s propicio para los accidentes es la cocina. La m&aacute;xima recomendaci&oacute;n es: nunca cocinar en ba&ntilde;ador, incluso estando fondeados. Es aconsejable llevar puesto un delantal y calzado.<br><br>En navegaci&oacute;n, nada debe ir sobre los fogones encendidos sin sujetar. En navegaci&oacute;n, sobre todo con mal tiempo todo debe estar estibado y trincado adecuadamente. Los objetos que no est&eacute;n ï¬jados correctamente pueden convertirse en proyectiles. La gran norma para evitar golpes y accidentes en el interior del barco es caminar utilizando los pasamanos o puntos de ï¬jaci&oacute;n dispuestos en toda la embarcaci&oacute;n.<br><br>Una vez m&aacute;s, hay que repetir que en las &aacute;reas cubiertas la limpieza es tan importante como en el exterior. Hay que evitar la acumulaci&oacute;n de cacharros en los fregaderos y no dejar frascos, aceiteras, azucareras sobre las superï¬cies de trabajo de la cocina y, sobre todo evitar comprar cosas envasadas en recipientes de vidrio. <br><br>Barco limpio no debe ser sin&oacute;nimo de mar sucio. Recoger todos los desperdicios en bolsas de basura y depositarlas siempre en tierra. Al mar no debe ir absolutamente nada. <br><br>En el ba&ntilde;o, los cabellos deben recogerse y echarse a la basura, no al suelo, pues pueden ostruir las bombas de achique. Seguir las instrucciones del patr&oacute;n al pie de la letra para abrir y cerrar las v&aacute;lvulas del WC puede parecer complicado pero en realidad es una maniobra sencilla. El WC es s&oacute;lo para cosas que han sido comidas o bebido previamente, nada m&aacute;s debe tirarse all&iacute;. Economizar agua en la ducha evitar&aacute; p&eacute;rdidas de tiempo en puertos -siempre que se pueda- repostando, pero la norma indica que siempre que pueda es mejor y m&aacute;s c&oacute;modo utilizar los servicios de las marinas.<br><br>Lo que nunca hay que hacer<br><br>Si se est&aacute; abarloado en una marina, es norma no pasar al pantal&aacute;n a trav&eacute;s de la ba&ntilde;era del vecino. Es una violaci&oacute;n de su intimidad. Siempre hay que cruzar por la cubierta del barco al que estamos abarloados.<br><br>Aunque a los integrantes masculinos de la tripulaci&oacute;n les de pereza ir hasta el ba&ntilde;o a hacer sus necesidades, o se mareen, deben hacerlo. No hay situaci&oacute;n m&aacute;s peligrosa que un tripulante colgado de la borda para orinar. Si alguien est&aacute; mareado y necesita vomitar debe hacerlo, en lo posible hacia sotavento. <br><br>No hay que dejar sobre los bancos o las brazolas chaquetas, camisetas, toallas, gorras o pa&ntilde;uelos. Desaparecer&aacute;n irremediablemente por sotavento m&aacute;s tarde o m&aacute;s temprano. Nunca hay que pisar las velas cuando son arriadas o permanecen en cubierta. <br><br>Cuando se colabora en la tarea de recogida de defensas, no hay que dejarlas en cubierta sino depositarlas en su lugar de estiba.<br><br>Es extremadamente peligroso tumbarse sobre cabos de trabajo o que est&eacute;n trabajando. Nunca dejarlas sin adujar los cabos o amarras recogidas.<br><br>Prohibido saltar bruscamente sobre la cubierta, dejar herramientas sobre ella, arrastrar el ancla o la cadena sobre ella. Vigilar que el ancla no golpee el casco al izarla a bordo.<br><br>Las escotillas abiertas son causa de muchos accidentes. En navegaci&oacute;n, hay que cerrarlas siempre, en fondeo o en puerto pueden estar medio abiertas.<br><br>La mesa de cartas es el altar del patr&oacute;n. No puede estar ocupada por objetos innecesarios para la navegaci&oacute;n.<br><br>Quien duerme en las literas del sal&oacute;n, no debe dejar los sacos de dormir o las s&aacute;banas sin recoger. Es el espacio com&uacute;n por excelencia y debe poder ser disfrutado por todos. <br><br>Jam&aacute;s dejar toallas h&uacute;medas sobre los sof&aacute;s o literas.<br>'),
(9, 13, '0000-00-00', 0, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_menu`
--

CREATE TABLE IF NOT EXISTS `brain_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `brain_menu`
--

INSERT INTO `brain_menu` (`id`, `id_padre`) VALUES
(1, 1),
(3, 7),
(4, 12),
(5, 16),
(6, 21),
(7, 23),
(8, 27),
(9, 29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_menu_elemento`
--

CREATE TABLE IF NOT EXISTS `brain_menu_elemento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL DEFAULT '0',
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Volcado de datos para la tabla `brain_menu_elemento`
--

INSERT INTO `brain_menu_elemento` (`id`, `id_padre`, `id_tipo`, `nombre`, `fecha`, `activo`, `orden`) VALUES
(1, 0, 1, 'Menu Principal', '2013-02-24', '1', 1),
(2, 1, 2, 'Playa', '2013-02-24', '1', 1),
(3, 1, 2, 'Monta&ntilde;a', '2013-02-24', '1', 2),
(4, 1, 2, 'Camps', '2013-02-27', '1', 3),
(7, 0, 1, 'Informaci&oacute;n', '2013-06-15', '1', 2),
(8, 7, 2, 'Acerca de malabau', '2013-06-15', '1', 1),
(17, 16, 2, 'Facebook', '2013-06-24', '1', 1),
(9, 7, 2, 'C&oacute;mo funciona', '2013-06-24', '1', 2),
(10, 7, 2, 'Seguridad', '2013-06-24', '1', 3),
(11, 7, 2, 'Contacto', '2013-06-24', '1', 4),
(12, 0, 1, 'Accesos', '2013-06-24', '1', 3),
(13, 12, 2, 'Panel de control', '2013-06-24', '1', 1),
(14, 12, 2, 'Publica tu anuncio', '2013-06-24', '1', 2),
(15, 12, 2, 'Ayuda', '2013-06-24', '1', 3),
(16, 0, 1, 'Redes', '2013-06-24', '1', 4),
(18, 16, 2, 'Twitter', '2013-06-24', '1', 2),
(19, 16, 2, 'Instagram', '2013-06-24', '0', 3),
(20, 16, 2, 'Vimeo', '2013-06-24', '1', 4),
(21, 0, 1, 'Idiomas', '2013-06-24', '1', 5),
(22, 21, 2, 'Espanol', '2013-06-24', '1', 1),
(23, 0, 1, 'Otros', '2013-06-24', '1', 6),
(24, 23, 2, 'Regalos pack', '2013-06-24', '1', 1),
(25, 23, 2, 'Cupones descuento', '2013-06-24', '1', 2),
(26, 23, 2, 'Ofertas', '2013-06-24', '1', 3),
(27, 0, 1, 'Blog', '2013-06-24', '1', 7),
(28, 27, 2, 'Blog', '2013-06-24', '1', 1),
(29, 0, 1, 'Foot', '2013-07-05', '1', 8),
(30, 29, 2, 'Aviso legal', '2013-07-05', '1', 1),
(31, 29, 2, ' Condiciones de uso', '2013-07-05', '1', 2),
(32, 29, 2, 'Contrato proveedores', '2013-07-05', '1', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_menu_item`
--

CREATE TABLE IF NOT EXISTS `brain_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `texto` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `target` enum('_self','_blank') COLLATE utf8_unicode_ci NOT NULL DEFAULT '_self',
  `url_externo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#',
  `url_interno` int(11) NOT NULL DEFAULT '1',
  `interno` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Volcado de datos para la tabla `brain_menu_item`
--

INSERT INTO `brain_menu_item` (`id`, `id_padre`, `id_idioma`, `texto`, `target`, `url_externo`, `url_interno`, `interno`) VALUES
(1, 2, 1, 'Playa', '_self', 'http://www.nowalia.es/clientes/v2/categorias.php?id=1', 2, '0'),
(2, 2, 2, '', '_self', '#', 1, '0'),
(3, 3, 1, 'Monta&ntilde;a', '_self', 'http://www.nowalia.es/clientes/v2/categorias.php?id=2', 3, '0'),
(4, 3, 2, '', '_self', '#', 1, '0'),
(5, 4, 1, 'Camps', '_self', 'http://www.nowalia.es/clientes/v2/categorias.php?id=3', 4, '0'),
(6, 4, 2, '', '_self', '#', 1, '0'),
(10, 8, 2, 'About Malabau', '_self', '#', 1, '0'),
(9, 8, 1, 'Acerca de malabau', '_self', '#', 6, '1'),
(11, 9, 1, 'C&oacute;mo funciona', '_self', '#', 7, '1'),
(12, 9, 2, '', '_self', '#', 1, '0'),
(13, 10, 1, 'Seguridad', '_self', '#', 8, '1'),
(14, 10, 2, '', '_self', '#', 1, '0'),
(15, 11, 1, 'Contacto', '_self', '#', 9, '1'),
(16, 11, 2, '', '_self', '#', 1, '0'),
(17, 13, 1, 'Panel de control', '_self', 'http://www.nowalia.es/clientes/malabau/admin/', 1, '0'),
(18, 13, 2, '', '_self', '#', 1, '0'),
(19, 14, 1, 'Publica tu anuncio', '_self', '#', 1, '0'),
(20, 14, 2, '', '_self', '#', 1, '0'),
(21, 15, 1, 'Ayuda', '_self', '#', 1, '0'),
(22, 15, 2, '', '_self', '#', 1, '0'),
(23, 17, 1, 'Facebook', '_self', '#', 1, '0'),
(24, 17, 2, '', '_self', '#', 1, '0'),
(25, 18, 1, 'Twitter', '_self', '#', 1, '0'),
(26, 18, 2, '', '_self', '#', 1, '0'),
(27, 19, 1, 'Instagram', '_self', '#', 1, '0'),
(28, 19, 2, '', '_self', '#', 1, '0'),
(29, 20, 1, 'Vimeo', '_self', '#', 1, '0'),
(30, 20, 2, '', '_self', '#', 1, '0'),
(31, 22, 1, 'Espa&ntilde;ol', '_self', '#', 1, '0'),
(32, 22, 2, '', '_self', '#', 1, '0'),
(33, 24, 1, 'Regalos pack', '_self', '#', 1, '0'),
(34, 24, 2, '', '_self', '#', 1, '0'),
(35, 25, 1, 'Cupones descuento', '_self', '#', 1, '0'),
(36, 25, 2, '', '_self', '#', 1, '0'),
(37, 26, 1, 'Ofertas', '_self', '#', 1, '0'),
(38, 26, 2, '', '_self', '#', 1, '0'),
(39, 28, 1, 'Blog', '_self', '#', 1, '0'),
(40, 28, 2, '', '_self', '#', 1, '0'),
(41, 30, 1, 'Aviso legal', '_self', '#', 11, '1'),
(42, 30, 2, '', '_self', '#', 1, '0'),
(43, 31, 1, ' Condiciones de uso', '_self', '#', 12, '1'),
(44, 31, 2, '', '_self', '#', 1, '0'),
(45, 32, 1, 'Contrato proveedores', '_self', '#', 13, '1'),
(46, 32, 2, '', '_self', '#', 1, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_menu_tipos`
--

CREATE TABLE IF NOT EXISTS `brain_menu_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multiidioma` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `perfil_read` int(11) NOT NULL,
  `perfil_edit` int(11) NOT NULL,
  `perfil_create` int(11) NOT NULL,
  `perfil_delete` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `brain_menu_tipos`
--

INSERT INTO `brain_menu_tipos` (`id`, `nombre`, `multiidioma`, `perfil_read`, `perfil_edit`, `perfil_create`, `perfil_delete`) VALUES
(1, 'menu', '0', 5, 5, 5, 5),
(2, 'menu_item', '1', 5, 5, 5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_noticia`
--

CREATE TABLE IF NOT EXISTS `brain_noticia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entradilla` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `archivo_1` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_pagina`
--

CREATE TABLE IF NOT EXISTS `brain_pagina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL DEFAULT '0',
  `id_idioma` int(11) NOT NULL DEFAULT '0',
  `meta_titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `template` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pagina.php',
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'separadas, por, comas',
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Volcado de datos para la tabla `brain_pagina`
--

INSERT INTO `brain_pagina` (`id`, `id_padre`, `id_idioma`, `meta_titulo`, `url`, `template`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 1, '', 'inicio', 'portada.php', '', ''),
(2, 1, 2, '', '', 'pagina.php', 'separadas, por, comas', ''),
(3, 2, 1, '', 'playa', 'playa.php', '', ''),
(4, 2, 2, '', '', 'pagina.php', 'separadas, por, comas', ''),
(5, 3, 1, '', 'montana', 'actividad.php', '', ''),
(6, 3, 2, '', '', 'pagina.php', 'separadas, por, comas', ''),
(7, 4, 1, '', 'camps', 'pagina.php', '', ''),
(8, 4, 2, '', '', 'pagina.php', 'separadas, por, comas', ''),
(9, 6, 1, '', 'acerca-de-malabau', 'acercade.php', '', ''),
(10, 6, 2, '', '', 'pagina.php', 'separadas, por, comas', ''),
(23, 13, 1, '', 'condiciones-proveedores', 'condiciones2.php', 'separadas, por, comas', ''),
(11, 7, 1, '', 'como-funciona', 'comofunciona.php', '', ''),
(12, 7, 2, '', '', 'pagina.php', 'separadas, por, comas', ''),
(13, 8, 1, '', 'seguridad', 'pagina.php', '', ''),
(14, 8, 2, '', '', 'pagina.php', 'separadas, por, comas', ''),
(24, 13, 2, '', '', 'pagina.php', 'separadas, por, comas', ''),
(15, 9, 1, '', 'contacto', 'pagina.php', '', ''),
(16, 9, 2, '', '', 'pagina.php', 'separadas, por, comas', ''),
(17, 10, 1, '', 'ayuda', 'ayuda.php', '', ''),
(18, 10, 2, '', '', 'pagina.php', 'separadas, por, comas', ''),
(19, 11, 1, '', 'aviso-legal', 'aviso_legal.php', '', ''),
(20, 11, 2, '', '', 'pagina.php', 'separadas, por, comas', ''),
(21, 12, 1, '', 'condiciones_usuarios', 'condiciones.php', '', ''),
(22, 12, 2, '', '', 'pagina.php', 'separadas, por, comas', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_perfiles`
--

CREATE TABLE IF NOT EXISTS `brain_perfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `valor` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `brain_perfiles`
--

INSERT INTO `brain_perfiles` (`id`, `nombre`, `valor`) VALUES
(1, 'WEBMASTER', 1000000),
(2, 'ADMINISTRADOR', 100000),
(3, 'EDITOR', 10000),
(4, 'ADMIN_SHOP', 1000),
(5, 'USUARIO', 100),
(6, 'USUARIO_WEB', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_texto`
--

CREATE TABLE IF NOT EXISTS `brain_texto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `brain_texto`
--

INSERT INTO `brain_texto` (`id`, `id_padre`, `id_idioma`, `texto`) VALUES
(1, 5, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean congue \r\nmagna ut turpis porta eget ullamcorper est venenatis. Class aptent \r\ntaciti sociosqu ad litora torquent per conubia nostra, per inceptos \r\nhimenaeos. Maecenas venenatis, justo sed sagittis aliquet, magna urna \r\nvolutpat nunc, id porttitor lacus turpis non nisi. Sed ac ligula nec \r\nodio tincidunt porttitor id at libero. Donec mollis, mauris vitae \r\naliquam pulvinar, odio neque rhoncus magna, vitae vestibulum sapien \r\nlorem non elit. Fusce tempor eros eget lectus mollis nec ullamcorper \r\nurna vestibulum. Donec sit amet dapibus dolor. Pellentesque commodo \r\ncursus pulvinar.\r\n'),
(2, 5, 2, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_tipos`
--

CREATE TABLE IF NOT EXISTS `brain_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multiidioma` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `archivos` int(11) NOT NULL,
  `perfil_read` int(11) NOT NULL,
  `perfil_edit` int(11) NOT NULL,
  `perfil_create` int(11) NOT NULL,
  `perfil_delete` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `brain_tipos`
--

INSERT INTO `brain_tipos` (`id`, `nombre`, `multiidioma`, `archivos`, `perfil_read`, `perfil_edit`, `perfil_create`, `perfil_delete`) VALUES
(1, 'pagina', '1', 0, 5, 2, 1, 1),
(2, 'texto', '1', 0, 5, 3, 2, 2),
(3, 'listado', '0', 0, 5, 2, 2, 2),
(4, 'noticia', '1', 1, 5, 3, 3, 3),
(5, 'formulario_basico', '0', 0, 2, 2, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_usuario`
--

CREATE TABLE IF NOT EXISTS `brain_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_perfil` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `brain_usuario`
--

INSERT INTO `brain_usuario` (`id`, `id_perfil`, `nombre`, `login`, `pass`, `fecha`, `activo`, `orden`) VALUES
(1, 1, 'Webmaster', 'nando', '90eb8760c187a2097884ed4c9ffbb6a4', '2011-08-31', '1', 1),
(2, 1, 'Administrador web', 'admin', 'a5a30bc4c47888cd59c4e9df68d80242', '2011-08-31', '1', 2),
(3, 5, '', 'test', '098f6bcd4621d373cade4e832627b4f6', '2013-02-27', '1', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_usuario_ficha`
--

CREATE TABLE IF NOT EXISTS `brain_usuario_ficha` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `nombre_usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `localidad` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `provincia` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `brain_usuario_ficha`
--

INSERT INTO `brain_usuario_ficha` (`id`, `id_padre`, `nombre_usuario`, `email`, `apellidos`, `company`, `direccion`, `localidad`, `provincia`, `cp`, `telefono`) VALUES
(1, 1, 'Fernando', 'fernando@nowalia.com', '', '', '', '', '', '', '954 406 379'),
(2, 2, 'Administrador', 'info@nowalia.com', '', '', '', '', '', '', ''),
(3, 3, '', 'test@gmail.com', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brain_web_config`
--

CREATE TABLE IF NOT EXISTS `brain_web_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dominio` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `use_default_languaje` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `charset` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'utf8',
  `web_session_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'new_web_claqueta',
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'separadas, por, comas',
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `admin_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'repiso.fernando@gmail.com',
  `activo` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `brain_web_config`
--

INSERT INTO `brain_web_config` (`id`, `nombre`, `dominio`, `lang`, `use_default_languaje`, `charset`, `web_session_name`, `meta_keywords`, `meta_description`, `friendly_url`, `admin_email`, `activo`) VALUES
(1, 'MALABAU', 'www.nowalia.es/clientes/malabau', 'es', '0', 'utf-8', 'malabau', 'keywords', 'description', '0', 'fernando@laclaqueta.com', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sender` int(11) NOT NULL,
  `id_receiver` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escuela`
--

CREATE TABLE IF NOT EXISTS `escuela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `nombre` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `direccion` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `ciudad` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `pais` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `telefono` varchar(14) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `logo` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `escuela`
--

INSERT INTO `escuela` (`id`, `id_user`, `tipo`, `nombre`, `descripcion`, `direccion`, `ciudad`, `pais`, `telefono`, `email`, `logo`) VALUES
(1, 1, 1, 'sdf', 'asdf', '', 'Tamil Nadu', ' India', '123456456', 'a28rangel@gmail.com', 'japon-9400941329.jpg'),
(2, 2, 0, '', '', '', '', '', '', '', 'icono-nowalia-8005230328.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fbusers`
--

CREATE TABLE IF NOT EXISTS `fbusers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fbuserid` varchar(18) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `rank` decimal(1,0) DEFAULT '1',
  `perms` varchar(600) DEFAULT NULL,
  `ip_visit` varchar(15) DEFAULT NULL,
  `dtreg` int(11) NOT NULL,
  `dtvisit` int(11) NOT NULL,
  `visits` smallint(5) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `friends`
--

CREATE TABLE IF NOT EXISTS `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_user_friend` int(11) NOT NULL,
  `new_user_id` int(11) NOT NULL,
  `status` binary(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `friends`
--

INSERT INTO `friends` (`id`, `id_user`, `id_user_friend`, `new_user_id`, `status`, `active`) VALUES
(1, 1, 2, 0, '\0', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logattempt`
--

CREATE TABLE IF NOT EXISTS `logattempt` (
  `email` varchar(55) NOT NULL,
  `nri` tinyint(3) unsigned DEFAULT '0',
  `ip` varchar(15) DEFAULT NULL,
  `dt` int(11) DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `logattempt`
--

INSERT INTO `logattempt` (`email`, `nri`, `ip`, `dt`) VALUES
('andres@nowalia.com', 0, '95.20.155.121', 1373386821);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `magazine`
--

CREATE TABLE IF NOT EXISTS `magazine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `magazine`
--

INSERT INTO `magazine` (`id`, `numero`, `fecha`, `nombre`, `titulo`, `imagen`, `orden`, `activo`) VALUES
(1, 1, '2013-01-01', 'nautica magazine enero', 'nautica magazine enero', '2013_01_portada.jpg', 1, 1),
(2, 2, '2013-02-01', 'nautica magazine febrero', 'nautica magazine febrero', '2013_02_portada.jpg', 2, 1),
(3, 3, '2013-03-01', 'nautica magazine marzo', '', '', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `magazine_noticias`
--

CREATE TABLE IF NOT EXISTS `magazine_noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_padre` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `visibilidad` int(11) NOT NULL,
  `seccion` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entradilla` text COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `magazine_noticias`
--

INSERT INTO `magazine_noticias` (`id`, `id_padre`, `fecha`, `visibilidad`, `seccion`, `titulo`, `nombre`, `entradilla`, `imagen`, `contenido`, `orden`, `activo`) VALUES
(1, 1, '0000-00-00', 0, 'NOTICIAS', 'El Telef&oacute;nica contin&uacute;a haciendo historia', 'noticia1', 'El velero de Iker Mart&iacute;nez logra su tercer triunfo en la vuelta al mundo. Pleno de victorias en las etapas de esta edición de la Volvo Ocean Race.', '2013_01_noticias01.jpg', '<p>El Telef&oacute;nica atraves&oacute; la l&iacute;nea de meta de la tercera etapa de la vuelta al mundo el s&aacute;bado 4 de febrero a las 4:58 hora peninsular. Es su tercera victoria de etapa en esta Volvo Ocean Race. Con ella acumula tres triunfos en otras tantas mangas.</p>\r\n<p>Aunque lo parezca, conseguir este triplete de triunfos no ha sido tarea f&aacute;cil. Ni muchos menos. "&Eacute;sta ha sido la etapa m&aacute;s peligrosa que he hecho en mucho tiempo", confesaba Iker Mart&iacute;nez. "Hemos navegado entre barcos, muy cerca de la costa, en aguas poco profundas y con un mont&oacute;n de barcos de pescadores. Ha sido muy estresante. Estamos muy contentos", aﬁrmaba el patr&oacute;n.</p>\r\n<p>Han pasado 22 a&ntilde;os desde un comienzo de vuelta al mundo tan arrollador. Hay que remontarse a la edici&oacute;n 1989/1990 para encontrar una supremac&iacute;a tan grande. Fue cuando el legendario Steinlager 2 de Sir Peter Blake se alz&oacute; con las seis etapas de la competici&oacute;n. Desde entonces, nadie hab&iacute;a logrado ganar las tres primeras etapas.</p>\r\n<p><strong>Victoria complicada</strong></p>\r\n<p>Tres horas despu&eacute;s de iniciar en Maldivas el segundo tramo de la etapa 3, el Telef&oacute;nica sufri&oacute; la rotura de su c&oacute;digo 0. Debido a ello y a otros da&ntilde;os en cubierta que provoc&oacute; el incidente, el velero español retrocedi&oacute; de la tercera a la sexta plaza de la clasiﬁcaci&oacute;n. Sin embargo, el barco dise&ntilde;ado por Juan Kouyoumdjian volvi&oacute; a demostrar que es el más r&aacute;pido de la ﬂota y antes de adentrarse en el temido estrecho de Malaca ya encabezaba la clasiﬁcación de la etapa. Los de Iker Mart&iacute;nez no cometieron fallos a la hora de escoger la mejor opción t&aacute;ctica y de sortear desde enormes mercantes a min&uacute;sculos barcos de pescadores e incluso tornados.</p>\r\n<p>En la &uacute;ltima parte de la etapa, ya en el mar de China, los seguidores comenzaron una interminable guerra de viradas con el ﬁn de dar caza al l&iacute;der. Una aut&eacute;ntica batalla con vientos de 30 nudos y olas de hasta tres metros.</p>                    <p>Pero ni los rivales (el Groupama acech&oacute; hasta las &uacute;ltimas millas de la etapa) ni las condiciones meteorol&oacute;gicas lograron cambiar el comienzo de esta historia, que bien puede convertirse en leyenda.</p>                                   ', 1, 1),
(2, 1, '0000-00-00', 0, 'NOTICIAS', 'La Euro Laser Masters Cup 2013 va viento en popa.', 'noticia2', 'Importante preinscripción de regatistas franceses y holandeses en el Euro Laser Masters Cup 2013-V Trofeo Calella de Palafrugell, Memorial Pitus Jiménez, que se celebrará del 18 al 21 de abril. Entre ellos el holandés Cesar Sierhuis, ganador de la edición 2011,que volverá a Calella de Palafrugell a revalidar el título.', '2013_01_noticias02.jpg', '<p>La cita gerundense de esta prueba europea se ha convertido en una clásica del circuito europeo y una regata referente de la clase laser a nivel internacional. A falta de algo más de dos meses del inicio de las pruebas, ya se han inscrito al Euro Laser Masters Cup del Club Vela Calella una cincuentena de regatistas, procedentes de Francia, Países Bajos, Bélgica, Gran Bretaña, Alemania, Suiza y España.</p>\r\n                    <p>Entre los regatistas franceses que participaran\r\nen la prueba están los dos veteranos Great Grand Master, De Roﬃgnac y Rene Roche, del CM Hendaye, los Grand Master Jacques Sicard y Falque Olivie del COYCH de Hieres y el Master del mismo club provenzal, Bernaz. Los franceses, siempre competitivos, tiene puestas sus miras en ganar en alguna de las clasiﬁcaciones de la EuroLaser Masters Cup.</p>\r\n                    <p>Los grandes veteranos holandeses tampoco van a faltar en la prueba del Club Vela Calella, y ya está inscrito un nutrido grupo de regatistas, encabezados por Cesar Sierhuis que intentará conquistar el título de nuevo. Junto con el campeón del pasado año también regatearán en Calella el Great Grand Master Henk Wittenberg, así como otros destacados laseristas holandeses, los Grand Master, Jaap Mazereeuw y Hans Holterman, no pudiendo faltar el Master Wilmar Groenendijk.</p>\r\n                    <p>Destacada será la participación de otros regatistas europeos, entre ellos el británico David Schoﬁeld, asiduo en este Trofeo, el belga Luc Dumonceau, el alemán Rene Bright o el suizo Broye.</p>\r\n                    <p>También la inscripción femenina se anima. Se contará en esta edición del Trofeo Calella de Palafrugell con la participación de la alemana Petra Busch y d la francesa Germnanie Larroque, que ya participó el año pasado.</p>                \r\n					<p><strong>Buena pre-inscripción española</strong></p>\r\n                    <p>La presencia española también promete ser numerosa. A parte de los regatistas locales del CV Calella, encabezados por el Great Grand Master, Pere Jimenez- Maifrén, navegarán en esta edición Miquel Álvarez, Josep Ferrés Marcó, Ignasi Jiménez-Gusí, Josep Maria Campi, Xavier Penas. Del CN Aiguablava viene Josep Maria Pujada.</p>\r\n                    <p>mportante la participación del grupo de laseristas procedentes del GEN de Roses, con Jordi Roca, Ernest Amat, Peter Muller, Toni Gibert ya asiduos en el circuito del Master Laser, así como Joan Grau del CN Vilassar de Mar, Miguel Noguer del CN Masnou, Xavier Marrón del CN Betulo y los regatistas del CN el Balis, Ricard Llonch y Xavier Boluda, los alicantinos José Juan Sánchez del CN Santa Pola, y David Quesada del CN Calpe, y el asturiano Rene Velázquez.</p>                                     ', 2, 1),
(3, 1, '0000-00-00', 0, 'BARCOS', 'Hallberg Rassy 64', 'noticia3', 'El nuevo Hallberg Rassy 64 presentado este año en el salón de Düsseldorf destaca por la seguridad y el confort a bordo que caracterizan a todos los barcos de este astillero.Diseñado por Germán Frers, es con un concepto de amplios espacios, luminosidad y visibilidad.', '2013_01_noticias03.jpg', '<p>Que el propio dueño de un astillero de los mejor reputación del mundo, en este caso Magnus Rassy, se enorgullezca de uno de los barcos salidos de su factoría, en este caso su HR 64, ya dice mucho. Si además, este último modelo salido de la casa sueca se convierte inmediatamente en el buque insignia en una marca tan prestigiosa y reputada como Hallberg Rassy, poco queda por agregar. Como en cualquier HR, éste 64 pies responde a los cánones de simplicidad y lujo náutico moderado, por lo que sorprenden más sus generosos espacios que su estética interna y externa, que está a la altura de las habituales en los veleros de este astillero.</p>\r\n					<p>Donde sí llama la atención es en navegación. Si bien todos los Hallberg Rassy son excelentes navegantes, éste es particularmente veloz y ligero, lo que es muy importante y un verdadero logro en un yate de casi 20 metros de largo. </p>\r\n                    <p>Su maniobrabilidad sorprende desde el propio pantalán. Una mano en la rueda, otra en el gas y con los pies se gobierna la hélice de proa. Sí, un botón en el suelo permite mover la proa a babor y otro botón hace lo propio para virar a estribor. No se trata de una genialidad, pero sí asombra que no se le haya ocurrido a nadie antes.</p>\r\n                    <p><strong>Cada milímetro es acertado</strong></p>\r\n                    <p>La bañera central es un decálogo de buenas soluciones diseñadas con un doble fin: comodidad y seguridad. Pero todo ello, como ya hemos dicho, sin comprometer las prestaciones, ya que sobre el agua es un barco atlético, versátil, seguro y cómodo. Gran parte de este mérito es del diseñador argentino Germán Frers, quien desde 1988 trabaja para la casa de Hallberg-Rassy. Esos 23 años le han permitido evolucionar conjuntamente con Magnus Rassy en el desarrollo de barcos que se superan ejemplar tras ejemplar.</p>\r\n                    <p>Dispone de una cubierta limpia y despejada. Las escotillas y tragaluces están empotrados y el cabestrante del ancla queda oculto bajo el nivel de cubierta.En su diseño se contemplan las opciones para enrollador hidráulico, cutterstay y enrollador de génova bajo cubierta.</p>\r\n                    <p>Una pasarela opcional se puede plegar en la bañera. En popa hay un espacio de 8,6 metros cúbicos que puede almacenar una semirrída y una plataforma de baño doble hidráulica</p>\r\n                    <p>Bajo cubierta es un verdadero gran yate. Tiene toda la amplitud de una casa. La altura de pie es de entre 2,05 y 2,15 metros en todo el interior. Los grandes portillos que permite esta altura interior traen mucha luz tanto al salón como a las cabinas, donde lo único que no cabe es la palabra estrechez. Sirve de ejemplo que la cama del camarote principal mide 2,05 por 1,70 metros. </p>\r\n                    <p>Todos los detalles muestran solidez y funcionalidad.\r\nDestaca la limpieza de los elementos y la generosa dimensión de cada uno de ellos. Desde el cableado de la electricidad y electrónica a la fontanería. Lo mismo puede decirse de la iluminación y la calefacción. O hasta de los toalleros de los baños.</p>\r\n                    <p><strong>Atlético y veloz</strong></p>\r\n                    <p>El HR 64 es un yate para grandes travesías y en cualquier tipo de mar. En viento fresco, su parabrisas brinda una protección absoluta y el timón responde con precisión y fineza. Con 18 nudos de viento es fácil superar los 10 nudos de velocidad y aún en esas condiciones es posible gobernar el barco con una sola mano. Todos los hidráulicos están al alcance del patrón que no necesita de ayuda extra. Como debe ser en un barco de travesías en las que no siempre hay cuatro brazos en las guardias y casi ninguno de ellos tiene la potencia de la extremidad de un gimnasta.</p>\r\n                    <p>Las polares marcan 9,2 nudos de velocidad del barco con 10 nudos de viento real y con un ángulo de 75º. Con trinqueta y mayor a 25 nudos de viento real y a 110º de ángulo, la embarcación puede alcanzar los 11,5 nudos.</p>\r\n                    <p>La motorización de serie en el Hallberg Rassy 64 es un Volvo Penta D6-280 de 280 caballos, con un depósito de combustible con capacidad para 1.800 litros y los tanques de agua dulce albergan 1.300 litros. Todo lo necesario para zarpar y no regresar por mucho tiempo. Y lo que es más importante, sin añorar nada de lo que quede en tierra.</p>', 3, 1),
(4, 1, '0000-00-00', 1, 'CHARTER', 'Datos claves para el alquiler de barcos I', 'noticia4', 'Las fechas, la zona de navegación, la elección del barco, la reserva y la ﬁrma del contrato son los primeros pasos. Saber cuáles son las actividades complementarias que se desean realizar y seguir algunas normas básicas de la vida a bordo serán indispensables para disfrutar de las vacaciones al máximo.', '2013_01_noticias04.jpg', '<p>Una vez tomada la decisión de alquilar un barco para pasar las vacaciones es importante hacer un plan con los pasos a seguir para la concreción de la idea. Sobre todo, cuando es la primera vez que se opta por este tipo de vacaciones en las que muchos imprevistos, si no se los conoce, pueden complicar los planes iniciales. </p>\r\n                    <p>El primer paso es establecer las fechas disponibles y la zona de navegación. Las fechas pueden implicar una reducción sustancial en el precio a pagar si se pasa de una semana de temporada alta a una inferior. En ocasiones, esto implica simplemente partir una semana antes o una semana después de lo previsto. Este calendario será clave, aunque no decisivo, para elegir el área de navegación en base al clima de la zona. En algunas ocasiones, dependiendo de las actividades que se desean realizar -submarinismo, windsurf, esquí acuático o pesca- será la zona la que determine las fechas.</p>\r\n                    <p>Establecidos estos puntos ya se puede buscar la compañía de alquiler. Nunca hay que quedarse con el primer folleto. La competencia en el mundo del chárter es enorme y se pueden encontrar buenos precios y alguna oferta en todo momento. Por lo tanto, hay comparar lo más que se pueda antes de hacer la reserva. Es recomendable realizar esta reserva unos meses antes del chárter.</p>\r\n                    <p>Elegir el barco y ﬁrmar el contrato son los siguientes pasos. En estos puntos es primordial tener muy claro qué extras son necesarios para los programas de navegación y actividades complementarias que se desean realizar. Si bien es cierto que la mayoría de las empresas tienen disponibilidad de accesorios como para satisfacer la mayoría de las inquietudes, no sería la primera vez que una familia se vea obligada a remar del barco a la playa y de la playa al barco todas las vacaciones por no haber solicitado un fueraborda para el auxiliar con la debida anticipación.</p>\r\n                    <p><strong>Un patrón preparado</strong></p>\r\n                    <p>Tanto cuando se alquila la embarcación con patrón como cuando se prescinde de este profesional es muy importante respetar ciertas normas de seguridad y convivencia que evitarán accidentes y mal ambiente a bordo. Cuando haya patrón, será él el primer interesado en preservar estos valores por lo que es muy importante prestar atención a sus indicaciones tanto a la hora de embarcar, como todas las que puedan devenir durante la navegación.</p>\r\n                    <p>Para quienes alquilen el barco sin patrón debe quedar claro que ello no implicará un problema si, además de la titulación, se tienen los conocimientos correspondientes. Bastará seguir algunas normas e lementales y aplicar mucho sentido común frente a los imprevistos para que el viaje sea tan agradable como cada tripulante lo imaginó.</p>\r\n                    <p>Es muy importante que la tripulación esté informada y sea participativa. Cada persona a bordo del barco debe saber dónde está cada elemento de seguridad y cómo se utiliza. Un buen reparto de tareas y responsabilidades hará que todos se sientan útiles y disfruten  de la navegación.</p>\r\n                    <p>La prudencia es una gran consejera para todos los tripulantes. No vale la pena forzar ningún aparejo ni apurar hasta una costa con poco fondo por ahorrarse un bordo. Sobre todo hay que tener en cuenta que en los barcos de alquiler aparejo y maniobra están pensadas para el crucero y no para ser exigidos como si de una regata se tratase. Nunca hay que olvidar, además, que un barco de chárter pasa por muchas manos y que no siempre los clientes le dan a la embarcación el trato que debieran.</p>\r\n                    <p>Finalmente es aconsejable dejar de lado cualquier tipo de inhibiciones a la hora de llamar a la compañía si surgen problemas o si no está seguro de cómo funciona algún equipo. Ellos mismos preferirán una llamada preventiva que un desperfecto posterior. </p>\r\n                    <p><strong>Tripulantes adiestrados</strong></p>\r\n                    <p>La primera vez que se pisa un barco, y más si es un velero, la sensación que domina es la de inseguridad. No se sabe qué se puede pisar y qué no, por donde pasar, dónde ubicar los pertrechos, ni de dónde asirse. </p>\r\n                    <p>El tripulante deberá, sobre todo si es novato, hacer un pequeño esfuerzo para adaptarse a los usos y costumbres de a bordo. Aprender a usar un lenguaje nuevo y a denominar a cada objeto por su nombre. A los pocos días, palabras como babor, estribor o través, formarán parte de su vocabulario.</p>\r\n                    <p>Y casi sin darse cuenta lo que antes le parecía un mundo de extraterrestres donde nada es lo que parece ni se denomina como debe, se convertirá en un hogar donde es sumamente fácil vivir y relacionarse. Para facilitar este camino damos a continuación una descripción de los principales espacios del barco y de cómo desenvolverse en ellos.</p>', 4, 1),
(5, 2, '0000-00-00', 0, 'NOTICIAS', 'Iván Pastor, Plata en el Preolímpico de Brasil', 'noticia1', 'El winsdurﬁsta del equipo Movistar Iván Pastor, del CN Santa Pola, ha ganado la medalla de plata en el Preolímpico de Brasil tras lograr nueve podios parciales. La competición celebrada en el marco de la Semana Brasileña de Vela se disputó en aguas de Buzios (Brasil). Con un total de diez mangas disputadas, la cita preolímpica ﬁnalizó una jornada antes de las seis previstas.', '2013_02_noticias01.jpg', '<p>Tras recibir su medalla, Iván Pastor dijo estar satisfecho con los resultados obtenidos: "Estoy muy contento con el resultado ﬁnal. Hemos logrado completar diez mangas y todas han sido muy largas, por lo que físicamente han sido durísimas. Además, ha sido un gran entrenamiento porque hemos tenido vientos muy variados durante toda la semana". </p>\r\n<p>Este Preolímpico brasileño ha sido una gran oportunidad para Iván Pastor para poner en práctica todo lo entrenado en los últimos meses, de cara a los Juegos Olímpicos de Londres. Estar al ciento por ciento física y psicológicamente es ahora la máxima prioridad del alicantino. El podio ﬁnal lo completaron los brasileños Ricardo Bimba, que se colgó el oro y Alberto Lopes, que ﬁnalizó en tercera posición. </p>                ', 1, 1),
(6, 2, '0000-00-00', 0, 'NOTICIAS', 'Aumenta la venta de barcos', 'noticia2', 'Enero de 2013 muestra un crecimiento del 12 por ciento en las ventas de embarcaciones en España y un 73 por ciento en Cataluña respecto a enero de 2012. La Asociación de Industrias Náuticas (ADIN) promueve festivales del mar para estimular el mercado náutico.', '2013_02_noticias02.jpg', '<p>Después de bajadas continuas en las matriculaciones de embarcaciones en Cataluña y en el Estado español, el mes de enero de 2012 cierra con cifras positivas que podrían ser un esperado cambio de tendencia.</p>\r\n<p>El pasado mes de enero 2013, se han matriculado en Cataluña un total de 45 embarcaciones, lo que representa un crecimiento de un 73 por ciento respeto las 26 embarcaciones del mes de enero de 2012. En el Estado español se han matriculado un total de 249 embarcaciones, lo que representa un crecimiento de un 12 por ciento respecto a las 223 embarcaciones del mes de enero de 2012. </p>\r\n<p>Con los datos facilitados por la Dirección General de la Marina Mercante, ADIN ha elaborado un informe en el que dice que el 83 por ciento de las embarcaciones vendidas en Cataluña son de hasta ocho metros y el 95 por ciento son inferiores a 12 metros. En el Estado español el 87 por ciento son inferiores a ocho metros y el 96 por ciento inferiores a 12 metros. </p>\r\n<p>Ante la situación, la asociación de industrias, comercio y servicios náuticos ADIN, promoverá una serie de iniciativas para dinamizar el sector náutico, atraer a futuros compradores, impulsar las empresas dedicadas al sector y acercar la actividad náutica a aquellos que no son profesionales. </p>\r\n<p>Con esta premisa, ADIN conjuntamente con los ayuntamientos de cada localidad ha elaborado un calendario para celebrar un Festival del Mar que tendrá lugar en los puertos de Sant Feliu de Guíxols, Port Ginesta en Sitges y Cambrils durante el mes de mayo.</p>                ', 2, 1),
(7, 2, '0000-00-00', 0, 'TRAVESÍA', 'Un puente en la<br /> Costa Brava', 'noticia3', 'La Costa Brava ofrece inﬁnidad de rincones idílicos que se pueden disfrutar en una travesía corta en tiempo necesario, pero intensa en imágenes y experiencias. En sólo cuatro días se puede recorrer y visitar la mitad de la    Costa Brava. Unir Barcelona con las Islas Medas puede ser muy atractivo para navegantes noveles sin exponerse a los dominios de la tramontana.', '2013_02_noticias03.jpg', '<p>Unir la costa del Maresme, al norte de Barcelona, con la Costa Brava, es una gran travesía para quien comienza a hacer sus primeros bordos en la navegación de crucero. En sólo cuatro días se pueden visitar tres puertos, parar en muchas calas y ver algunos de los paisajes costeros más bonitos de la costa española.</p>\r\n<p>Todo ello con dos grandes alicientes para el navegante con poca experiencia: nunca se está alejado de la costa con lo que no se requiere una titulación elevada, y son sólo cien millas que se pueden dividir en siete u ocho tramos, mañana y tarde, por lo que nunca se sobrepasan las cuatro o cinco horas de navegación.</p>\r\n<p>La propuesta de llegar a Medas y no hasta el cabo de Creus, un destino a priori más apetecible, tiene un gran fundamento. De Medas a Creus, la tramontana comienza a hacerse notar y obliga a estar mucho más pendiente del parte meteorológico de lo habitual.</p>\r\n<p>Echado el vistazo a la meteo, y seguros de que el barco está en orden, se puede partir sin temores, ya que es una ruta muy segura en cuanto a puertos de recalada. La distancia más larga entre dos de ellos es de 15 millas entre Arenys y Blanes, por lo que el principiante siempre estará a una horita de un amparo seguro.</p>\r\n<p>Como para muestra basta un botón aquí exponemos la travesía que nos envió nuestra lectora Meritxell García.</p>                    \r\n<p><strong>El centro de la Costa Brava</strong></p>\r\n<p>El sábado por la mañana pusimos rumbo a Medas. Desayunamos en el pueblo, pero, como es habitual en estas fechas, había mucha gente aprovechando el puente por lo que preferimos huir de las multitudes, que era nuestro plan de ﬁn de semana. Hicimos unas millas a motor y tras dar una vuelta para ver la costa de Callella de Palafrugell, uno de los pueblos más emblemáticos de este litoral, pasamos entre Llafranc y las islas Formigas, para luego apagar motor y seguir a vela.</p>\r\n<p>Esta vez el viento nos daba entre la aleta de estribor y el través, por lo que los diez nudos de intensidad nos empujaron hacia Medas con alegría. Al pasar junto al cabo de Begur, corregimos rumbo para rodear las Medas Petitas por fuera para ver la costa norte del pequeño archipiélago y luego fuimos hacia la Roca Foradada.</p>\r\n<p>invernando en el garaje de mis padres. La tentación de pasar de un lado a otro de la montaña por el hueco que la naturaleza ha creado debajo sólo fue detenida a diez metros de la entrada cuando quedaba totalmente claro que el mástil no pasaba bajo ese puente natural. Dudamos entre quedarnos fondeados en la cala o ir a puerto. Pero nos apetecía darnos una ducha caliente y andar un poco por Estartit.</p>\r\n<p><strong>Pascua en Sa Riera</strong></p>\r\n<p>El domingo nos levantamos a las nueve y media. Estábamos a mitad de las vacaciones, pero la sensación de que todo se empieza a acabar cuando se va a iniciar el regreso no nos la quitaba nadie. Quizás por eso, en lugar de encarar directo al cabo de Begur pusimos proa a Sa Riera donde fondeamos.  </p>\r\n<p>Los bares estaban llenos, los encargados del pequeño club náutico comenzaban a bajar llaúts al agua para que estén a llaúts al agua para que estén a punto en las vacaciones, había gente tomando el sol y hasta algún osado bañista. Tomamos un aperitivo, comentamos los proyectos para el verano tras esta primera experiencia mía, y disfrutamos de unas horas de vacaciones como el entorno lo pedía. Hacia las cuatro pusimos proa a Sant Felíu, donde llegamos a motor sobre las ocho y media. Justo para comer un pescado en el Paseo Marítimo.</p>\r\n<p><strong>Última escala</strong></p>\r\n<p>Un poco por el ﬁn inexorable del viaje, otro por justiﬁcar la visita a la ciudad, nos levantamos a las siete para visitar la ermita de Sant Elm. La caminata era larga y la subida nos llevaría unas dos horas, pero desde allí veríamos el mar que nos separaba de casa. La vista es imponente e imperdible. Antes de las once de la mañana ya virábamos la ermita, esta vez por mar.</p>', 3, 1),
(8, 2, '0000-00-00', 1, 'CHARTER', 'Datos claves para el alquiler de barcos II', 'noticia4', 'Las fechas, la zona de navegación, la elección del barco, la reserva y la ﬁrma del contrato son los primeros pasos. Saber cuáles son las actividades complementarias que se desean realizar y seguir algunas normas básicas de la vida a bordo serán indispensables para disfrutar de las vacaciones al máximo.', '2013_02_noticias04.jpg', '<p>Una vez tomada la decisión de alquilar un barco para pasar las vacaciones es importante hacer un plan con los pasos a seguir para la concreción de la idea. Sobre todo, cuando es la primera vez que se opta por este tipo de vacaciones en las que muchos imprevistos, si no se los conoce, pueden complicar los planes iniciales. </p>\r\n                    <p><strong>Bañera</strong></p>\r\n                    <p>La bañera es el espacio de la cubierta destinado al gobierno y a la maniobra de la embarcación, pero es también el espacio de convivencia al aire libre por excelencia. Como el resto de la embarcación, es un espacio que debe permanecer siempre ordenado, con cada cosa en su lugar y libre de obstáculos que puedan, en un momento determinado hacer fracasar una maniobra o causar un accidente.</p>\r\n                    <p>Lo primero que hay que aprender es dónde está barlovento y dónde sotavento. Saber de dónde viene el viento hará comprender los movimientos del barco. Lo segundo a tener en cuenta es que si no se está completamente seguro de lo que se va a hacer, preguntar puede evitar un error que provoque un accidente.</p>\r\n                    <p>Entre las normas de convivencia a respetar destacan tres. La primera es evitar derramar comida o bebidas en cubierta y, si ello ocurre, limpiar inmediatamente. El suelo de las bañeras suele acabar repleto de cáscaras de pipas, pieles de cacahuete, trozos de patatas chips y de galletas o migas de pan. Cuando se moja, el conjunto se convierte en una desagradable pasta. En verano se suele andar descalzo y nadie tiene por qué pisar estos desperdicios.</p>\r\n                    <p>La segunda regla es mantener el orden. Es desagradable y peligroso que una bañera parezca un plato de espaguetis debido al desorden de cabos. Y es lamentable y caro perder por la borda gafas, cremas solares, botellas o manetas de winche por no haberlas arranchado como corresponde en alguno de los muchos cofres o guanteras que suele haber en esta parte del barco.</p>\r\n                    <p>La última pauta tiene que ver con la convivencia, pero también con la seguridad: nunca hay que dormir en cubierta. Se molesta a los que maniobran y se corre el riesgo de caer al agua.</p>\r\n                    <p>Para continuar con la seguridad, el tripulante novel debe saber que siempre deben estar a salvo de accidentes cabeza, manos y pies. </p>\r\n                    <p>Aunque en los veleros de chárter actuales la botavara suele quedar a suﬁciente altura, siempre hay que estar pendiente de viradas o trasluchadas repentinas para mantener lacabeza fuera de su recorrido.</p>\r\n                    <p>Si hablamos de las manos, el peligro está en los winches. Cuando se cazan o amollan escotas con la ayuda de estos elementos, hay que colocar la mano plana sobre ellos para no pillarse los dedos. Y para evitar accidentes en los pies es imprescindible no andar descalzo en la bañera. Eso no es recomendable en navegación. Los dedos y las uñas de los pies deben estar siempre protegidos. Además, con un buen calzado antideslizante se evitarán resbalones que pueden ﬁnalizar en golpes o caídas.</p>\r\n                    <p><strong>Cubierta</strong></p>\r\n                    <p>No hay dos cubiertas iguales. Eso vale, no solo para la distribución de la maniobra, sino también a la facilidad de transitar por ella o los espacios libres que pueden destinarse, por ejemplo, a tomar el sol. Hay que habituarse a circular por ella, saber dónde colocarse, conocer el acastillaje del que hay que mantenerse alejado y, sobre todo, aprender a mantener el equilibrio.</p>\r\n                    <p>Moverse por una cubierta plana, fondeados en una cala es fácil. Pero cuando el barco escora y cabecea y los rociones en las amuras son constantes, las cosas ya no son sencillas. Agarrarse a determinados puntos para no perder el equilibrio puede incluso llegar a ser peligroso. Por tanto, hay que saber que en todas las cubiertas, para desplazarse de popa a proa y viceversa, existen pasamanos sobre la caseta que conﬁeren un agarre seguro. La regla básica dice que se debe estar cogido a un punto ﬁjo siempre: en el charco una mano para ti y la otra para el barco. </p>\r\n                    <p>En esta zona del barco hay que estar siempre alejado de los puños de las velas. Con mal tiempo, nunca hay que deambular por cubierta sin arnés de seguridad. De noche, el arnés es preceptivo incluso con mar plana.</p>\r\n                    <p>Como en la bañera, aquí también es aconsejable ir con calzado antideslizante. Los momentos en los que son más frecuentes los accidentes en cubierta son durante fondeos y atraques, y en viradas y trasluchadas. Para evitar accidentes en los fondeos hay que apartarse siempre del recorrido de la cadena o del cabo al fondear el ancla. En el último tramo de izada del ancla, cuando ésta recupera su peso real si se la sube a mano hay que adoptar una posición correcta de la espalda. Durante los atraques nunca hay intentar impedir que el barco toque el muelle u otro barco con el pie o las manos.</p>\r\n                    <p>Y en las viradas y trasluchadas hay que mantenerse alejado del trayecto de la vela al pasar de una banda a otra y de los puños cuando el génova gualdrapea. Jamás hay que permanecer tendido en cubierta tomando el sol durante las maniobras y siempre es aconsejable estar detrás del palo.</p>\r\n                    <p>En cuanto a limpieza, en cubierta valen los mismos conceptos vertidos para la bañera. con una recomendación extra para los amantes del sol. Siempre hay tenderse a tomar el sol sobre una toalla. Las cremas solares convierten las cubiertas en verdaderas pistas de patinaje.</p>\r\n                    <p><strong>Interior</strong></p>\r\n                    <p>Mientras dure la singladura, el barco será el hotel y el restaurante de la tripulación. Eso signiﬁca dormir, comer, asearse y hacer todo lo que se haría en tierra, incluyendo los ratos de ocio, de lectura o de conversación en torno a la mesa. Aunque no lo parezca, dentro de un barco hay mucho más espacio del que parece y se puede hacer todo lo que uno haría si alquilase un apartamento para pasar sus vacaciones, pero es imprescindible saber encontrar el espacio propio y respetar el de los demás.</p>\r\n                    <p>En el interior del barco las estancias están muy bien delimitadas según los usos. En líneas generales en un barco destinado al chárter hay un espacio a proa destinado a camarotes y aseos, otro central dedicado a salón, cocina y mesa de cartas, y otro en popa también destinado a cabinas y aseo. Cada cosa debe tener su lugar de estiba especíﬁco y hay que respetarlo. Por tanto cada objeto que salga de un sitio debe volver al mismo después de su uso. El orden y la limpieza son fundamentales para a convivencia, al igual que lo es respetar los horarios de descanso.</p>\r\n                    <p>Lo primero que debe saber un tripulante novato es que la energía eléctrica es un bien escaso. A menos que se esté conectado a tierra o se disponga de un generador, no hay mantener las luces innecesariamente encendidas. No se debe bajar al salón mojado, y menos cuando recién se regresa de un chapuzón en una cala chorreando agua salada, o con arena en los pies. Las válvulas de los sistemas de achique pueden obstruirse y se deterioran con mucha facilidad.</p>\r\n                    <p>El segundo bien a cuidar es el agua dulce. Los recursos son limitados y, aunque se pueda repostar en un puerto cercano, en ocasiones, la operación no es sencilla. Sobre todo en verano. El espacio más propicio para los accidentes es la cocina. La máxima recomendación es: nunca cocinar en bañador, incluso estando fondeados. Es aconsejable llevar puesto un delantal y calzado.</p>\r\n                    <p>En navegación, nada debe ir sobre los fogones encendidos sin sujetar. En navegación, sobre todo con mal tiempo todo debe estar estibado y trincado adecuadamente. Los objetos que no estén ﬁjados correctamente pueden convertirse en proyectiles. La gran norma para evitar golpes y accidentes en el interior del barco es caminar utilizando los pasamanos o puntos de ﬁjación dispuestos en toda la embarcación.</p>\r\n                    <p>Una vez más, hay que repetir que en las áreas cubiertas la limpieza es tan importante como en el exterior. Hay que evitar la acumulación de cacharros en los fregaderos y no dejar frascos, aceiteras, azucareras sobre las superﬁcies de trabajo de la cocina y, sobre todo evitar comprar cosas envasadas en recipientes de vidrio. </p>\r\n                    <p>Barco limpio no debe ser sinónimo de mar sucio. Recoger todos los desperdicios en bolsas de basura y depositarlas siempre en tierra. Al mar no debe ir absolutamente nada. </p>\r\n                    <p>En el baño, los cabellos deben recogerse y echarse a la basura, no al suelo, pues pueden ostruir las bombas de achique. Seguir las instrucciones del patrón al pie de la letra para abrir y cerrar las válvulas del WC puede parecer complicado pero en realidad es una maniobra sencilla. El WC es sólo para cosas que han sido comidas o bebido previamente, nada más debe tirarse allí. Economizar agua en la ducha evitará pérdidas de tiempo en puertos -siempre que se pueda- repostando, pero la norma indica que siempre que pueda es mejor y más cómodo utilizar los servicios de las marinas.</p>\r\n                    <p><strong>Lo que nunca hay que hacer</strong></p>\r\n                    <p>Si se está abarloado en una marina, es norma no pasar al pantalán a través de la bañera del vecino. Es una violación de su intimidad. Siempre hay que cruzar por la cubierta del barco al que estamos abarloados.</p>\r\n                    <p>Aunque a los integrantes masculinos de la tripulación les de pereza ir hasta el baño a hacer sus necesidades, o se mareen, deben hacerlo. No hay situación más peligrosa que un tripulante colgado de la borda para orinar. Si alguien está mareado y necesita vomitar debe hacerlo, en lo posible hacia sotavento. </p>\r\n                    <p>No hay que dejar sobre los bancos o las brazolas chaquetas, camisetas, toallas, gorras o pañuelos. Desaparecerán irremediablemente por sotavento más tarde o más temprano. Nunca hay que pisar las velas cuando son arriadas o permanecen en cubierta. </p>\r\n                    <p>Cuando se colabora en la tarea de recogida de defensas, no hay que dejarlas en cubierta sino depositarlas en su lugar de estiba.</p>\r\n                    <p>Es extremadamente peligroso tumbarse sobre cabos de trabajo o que estén trabajando. Nunca dejarlas sin adujar los cabos o amarras recogidas.</p>\r\n                    <p>Prohibido saltar bruscamente sobre la cubierta, dejar herramientas sobre ella, arrastrar el ancla o la cadena sobre ella. Vigilar que el ancla no golpee el casco al izarla a bordo.</p>\r\n                    <p>Las escotillas abiertas son causa de muchos accidentes. En navegación, hay que cerrarlas siempre, en fondeo o en puerto pueden estar medio abiertas.</p>                \r\n                    <p>La mesa de cartas es el altar del patrón. No puede estar ocupada por objetos innecesarios para la navegación.</p>\r\n                    <p>Quien duerme en las literas del salón, no debe dejar los sacos de dormir o las sábanas sin recoger. Es el espacio común por excelencia y debe poder ser disfrutado por todos. </p>\r\n                    <p>Jamás dejar toallas húmedas sobre los sofás o literas.</p>', 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `msgs`
--

CREATE TABLE IF NOT EXISTS `msgs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(32) NOT NULL,
  `idusr` int(10) unsigned DEFAULT NULL,
  `name` varchar(32) DEFAULT '',
  `email` varchar(55) DEFAULT '',
  `fbuserid` varchar(18) DEFAULT '0',
  `social` varchar(6) DEFAULT '0',
  `msg` varchar(750) DEFAULT '',
  `dt` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(15) DEFAULT '',
  `amail` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Volcado de datos para la tabla `msgs`
--

INSERT INTO `msgs` (`id`, `user`, `idusr`, `name`, `email`, `fbuserid`, `social`, `msg`, `dt`, `ip`, `amail`) VALUES
(1, 'arangel', 2, '', 'arangeltorres@gmail.com', '0', '0', '', 1372145236, '85.48.199.15', 2),
(2, 'a28rangel', 3, '', 'a28rangel@gmail.com', '0', '0', '', 1372179843, '79.153.234.166', 2),
(3, 'juanma', 4, '', 'jnmgonzalez@hotmail.com', '0', '0', '', 1372181135, '80.58.250.73', 2),
(4, 'a28rangel', 5, '', 'noname@hotmail.com', '0', '0', '', 1372181440, '79.153.234.166', 2),
(5, 'a28rangel', 6, '', 'a28rangel@gmail.com', '0', '0', '', 1372182216, '79.153.234.166', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `register_type`
--

CREATE TABLE IF NOT EXISTS `register_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `register_type`
--

INSERT INTO `register_type` (`id`, `type`) VALUES
(1, 'Nautica'),
(2, 'Facebook'),
(3, 'Twitter');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_de_actividad`
--

CREATE TABLE IF NOT EXISTS `tipos_de_actividad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_bloques_actividades` int(11) NOT NULL,
  `nombre` varchar(256) COLLATE latin1_spanish_ci NOT NULL,
  `imagen_1` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `imagen_2` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `tipos_de_actividad`
--

INSERT INTO `tipos_de_actividad` (`id`, `id_bloques_actividades`, `nombre`, `imagen_1`, `imagen_2`, `descripcion`) VALUES
(1, 1, 'Windsurf', 'playa_act_1.jpg', 'windsurf.jpg', ''),
(2, 1, 'Kitesurf', 'mini-kitesurf.jpg', 'kitesurf.jpg', ''),
(3, 1, 'Surf', 'mini-surf.jpg', 'surf.jpg', ''),
(4, 1, 'Paddle', 'mini-paddle.jpg', 'paddle.jpg', ''),
(5, 1, 'Vela', 'mini-vela.jpg', 'vela.jpg', ''),
(6, 1, 'Submarinismo', 'playa_act_6.jpg', 'submarinismo.jpg', ''),
(7, 2, 'Snow', 'mini-snow.jpg', 'snow.jpg', ''),
(8, 2, 'Esqui', 'mini-esqui.jpg', 'esqui.jpg', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `useron`
--

CREATE TABLE IF NOT EXISTS `useron` (
  `email` varchar(55) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `sid` char(50) DEFAULT NULL,
  `dt` int(11) DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `name` varchar(32) DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `passenc` varchar(32) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `rank` decimal(1,0) DEFAULT '0',
  `ip_reg` varchar(15) DEFAULT NULL,
  `ip_visit` varchar(15) DEFAULT NULL,
  `dtreg` int(11) NOT NULL,
  `dtvisit` int(11) NOT NULL,
  `visits` smallint(5) unsigned DEFAULT '0',
  `pass` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`name`, `id`, `passenc`, `email`, `rank`, `ip_reg`, `ip_visit`, `dtreg`, `dtvisit`, `visits`, `pass`) VALUES
('andres', 1, '32250170a0dca92d53ec9624f336ca24', 'andres@nowalia.com', 9, '127.0.0.1', '95.20.155.121', 1371581959, 1373386821, 101, 'pass123'),
('arangel', 2, '32250170a0dca92d53ec9624f336ca24', 'arangeltorres@gmail.com', 1, '85.48.199.15', '95.20.155.121', 1372145234, 1372960105, 16, 'pass123'),
('juanma', 4, 'fcea920f7412b5da7be0cf42b8c93759', 'jnmgonzalez@hotmail.com', 1, '80.58.250.73', '80.58.250.73', 1372181135, 0, 0, '1234567'),
('a28rangel', 6, 'fcea920f7412b5da7be0cf42b8c93759', 'a28rangel@gmail.com', 1, '79.153.234.166', '79.153.234.166', 1372182216, 0, 0, '1234567');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usersdat`
--

CREATE TABLE IF NOT EXISTS `usersdat` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(256) DEFAULT NULL,
  `apellidos` varchar(256) DEFAULT NULL,
  `sexo` int(11) NOT NULL,
  `direccion` varchar(256) NOT NULL,
  `pais` varchar(256) DEFAULT NULL,
  `ciudad` varchar(256) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `bday` date DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `descripcion` text,
  `img` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usersdat`
--

INSERT INTO `usersdat` (`id`, `nombre`, `apellidos`, `sexo`, `direccion`, `pais`, `ciudad`, `email`, `bday`, `telefono`, `descripcion`, `img`) VALUES
(1, 'Andres', 'Rangel', 1, 'Calle Camino de Cantalobos', '   España', '   Espartinas', 'andres@nowalia.com', '1992-02-01', '647583630', 'descripcion', 'icono-nowalia-616582119.png'),
(2, NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, 'logotipo-nowalia-9614355470.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_files`
--

CREATE TABLE IF NOT EXISTS `user_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `file_name` varchar(60) NOT NULL,
  `file_title` varchar(60) NOT NULL,
  `file_size` int(11) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `user_files`
--

INSERT INTO `user_files` (`id`, `id_parent`, `file_name`, `file_title`, `file_size`, `uploaded_date`) VALUES
(4, 11, 'imagen4-1275374395.jpg', 'imagen4-1275374395.jpg', 25782, '2013-07-05 09:11:06'),
(5, 11, 'imagen8-8975906786.jpeg', 'imagen8-8975906786.jpeg', 88932, '2013-07-05 09:12:14'),
(6, 11, 'logotipo-nowalia-3619613535.jpg', 'logotipo-nowalia-3619613535.jpg', 501597, '2013-07-08 19:50:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_list_friends`
--

CREATE TABLE IF NOT EXISTS `user_list_friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_friend` int(11) NOT NULL,
  `id_user_list` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `user_types`
--

INSERT INTO `user_types` (`id`, `type`) VALUES
(1, 'empleado'),
(2, 'general_user');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
