package com.laclaqueta.detiending.utilities;

import android.content.Context;
import android.content.Intent;

public final class CommonUtilities {
	
    //public static final String SERVER_URL = "http://detiending.com/gcm/register.php";
	public static final String SERVER_URL = "http://detiending.com/gcm/gcm/index.php";
    /*
     * GOOGLE PROJECT ID
     * */
    public static final String SENDER_ID = "600354756946";
    public static final String DISPLAY_MESSAGE_ACTION = "com.laclaqueta.detiending.DISPLAY_MESSAGE";

    public static final String EXTRA_MESSAGE = "message";
    public static final String TAG = "GCM";

    public static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
}
