package com.laclaqueta.detiending.main;

import static com.laclaqueta.detiending.utilities.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.laclaqueta.detiending.utilities.CommonUtilities.EXTRA_MESSAGE;
import static com.laclaqueta.detiending.utilities.CommonUtilities.SENDER_ID;
import static com.laclaqueta.detiending.utilities.CommonUtilities.SERVER_URL;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.laclaqueta.detiending.R;
import com.laclaqueta.detiending.manager.AlertDialogManager;
import com.laclaqueta.detiending.manager.ConnectionDetector;
import com.laclaqueta.detiending.utilities.CommonUtilities;
import com.laclaqueta.detiending.utilities.ServerUtilities;
import com.laclaqueta.detiending.utilities.WakeLocker;



public class MainActivity extends Activity {
	AlertDialogManager alert = new AlertDialogManager();
	ConnectionDetector cd;
	TextView lblMessage;
	AsyncTask<Void, Void, Void> mRegisterTask;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		cd = new ConnectionDetector(getApplicationContext());
		if (!cd.isConnectingToInternet()) {
			alert.showAlertDialog(MainActivity.this, "Error en la conexi�n de internet", "Conecta a una conexi�n de internet activa por favor", false);
			return;
		}

		if (SERVER_URL == null || SENDER_ID == null || SERVER_URL.length() == 0 || SENDER_ID.length() == 0) {
			alert.showAlertDialog(MainActivity.this, "Error de Configuraci�n!", "Configura los par�metros del servidor, por favor", false);
			 return;
		}else{
			//alert.showAlertDialog(MainActivity.this, "Server_url", SERVER_URL, false);
		}
		lblMessage = (TextView) findViewById(R.id.lblMessage);
		
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));
	
		
		final String regId = GCMRegistrar.getRegistrationId(this);
		Log.i("MAIN_ACTIVITY ---------------------- ", "regid " + regId);
		if (regId.equals("")) {
			GCMRegistrar.register(this, SENDER_ID);
		} else {
			if (GCMRegistrar.isRegisteredOnServer(this)) {
				Toast.makeText(getApplicationContext(), "Ya estas registrado con GCM", Toast.LENGTH_LONG).show();
			} else {
				final Context context = this;
				mRegisterTask = new AsyncTask<Void, Void, Void>() {

					@Override
					protected Void doInBackground(Void... params) {
						ServerUtilities.register(context, "", "", regId);
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						mRegisterTask = null;
					}
				};
				mRegisterTask.execute(null, null, null);
			}
		}
	}
	
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
			WakeLocker.acquire(getApplicationContext());
			
			/**
			 * 
			 * TODO: Aqu� es donde tenemos que tomar una decisi�n de respuesta
			 * 		una vez recibido el mensaje.
			 * 
			 * */
			
			lblMessage.append(newMessage + "\n");			
			Toast.makeText(getApplicationContext(), "Nuevo mensaje: " + newMessage, Toast.LENGTH_LONG).show();
			WakeLocker.release();
		}
	};
	
	@Override
	protected void onDestroy() {
		if (mRegisterTask != null) {
			mRegisterTask.cancel(true);
		}
		try {
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(this);
		} catch (Exception e) {
			Log.e("Liberar registro ha producido un error", "> " + e.getMessage());
		}
		super.onDestroy();
	}
}
